/**
 * Application view's package.
 * View is the visible part of the application in the MVC model.
 */
package genephrase.view;