package genephrase.view.window;

import genephrase.controller.window.StructureController;
import genephrase.model.NoLemmasException;
import genephrase.model.lang.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.*;

/**
 * The structure dialog, to define the generated sentences' structure.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class StructureDialog extends JDialog {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	/** The current SentenceGenerator. **/
	private SentenceGenerator stg;
	/** The local SentenceGenerator (to show example sentence). **/
	private SentenceGenerator localStg;
	/** The modified sentence structure. **/
	private ArrayList<Character> tmpStructure;
	/** The current MainWindow. **/
	private MainWindow mw;
	//Widgets
	/** The panel which contains all the dialog's content (to set up a border). **/
	private JPanel pContent = new JPanel();
	/** Header label. **/
	private JLabel lStructure = new JLabel("Cliquez sur un type de mot pour l'ajouter à la structure de la phrase.");
	/** The textfield which contains the modified structure. **/
	private JTextField tfStructure = new JTextField();
	/** The delete button, to remove the last type of word in the sentence. **/
	private JButton bDelete = new JButton("<-");
	/** The panel which contains structure textfield and delete button. **/
	private JPanel pStructure = new JPanel(new BorderLayout(5,5));
	/** The panel which contains type of words buttons. **/
	private JPanel pButtons = new JPanel(new GridLayout(2,4,5,5));
	/** The proper name button. **/
	private JButton bProperName = new JButton("Nom propre");
	/** The common name button. **/
	private JButton bCommonName = new JButton("Nom commun");
	/** The verb button. **/
	private JButton bVerb = new JButton("Verbe");
	/** The preposition button. **/
	private JButton bPrep = new JButton("Préposition");
	/** The adjective button. **/
	private JButton bAdjective = new JButton("Adjectif");
	/** The complement button. **/
	private JButton bComp = new JButton("Complément");
	/** The adverb button. **/
	private JButton bAdverb = new JButton("Adverbe");
	/** The label to indicate the example sentence. **/
	private JLabel lExample = new JLabel("Exemple de phrase :");
	/** The label which contains the example sentence. **/
	private JLabel lSentenceExample = new JLabel("Ajoutez un type de mot à la structure !");
	/** The OK button. **/
	private JButton bOk = new JButton("OK");
	/** The cancel button. **/
	private JButton bCancel = new JButton("Annuler");
	/** The footer panel, which contains the OK and cancel buttons. **/
	private JPanel pFooter = new JPanel(new GridLayout(1,2,5,5));

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public StructureDialog(SentenceGenerator stg, MainWindow mw) {
		super();
		this.stg = stg;
		this.mw = mw;
		resetLocalStg();
		setTitle("Éditer la structure de phrase");
		setSize(600,300);
		setVisible(false);
		createInterface();
		updateReadableStructure();
		addReactions();
	}

//ACCESSORS
	//WIDGETS
	/**
	 * Returns the proper name button.
	 */
	public JButton getBproperName() {
		return bProperName;
	}

	/**
	 * Returns the common name button.
	 */
	public JButton getBcommonName() {
		return bCommonName;
	}

	/**
	 * Returns the verb button.
	 */
	public JButton getBverb() {
		return bVerb;
	}

	/**
	 * Returns the preposition button.
	 */
	public JButton getBprep() {
		return bPrep;
	}

	/**
	 * Returns the adjective button.
	 */
	public JButton getBadjective() {
		return bAdjective;
	}

	/**
	 * Returns the complement button.
	 */
	public JButton getBcomp() {
		return bComp;
	}

	/**
	 * Returns the adverb button.
	 */
	public JButton getBadverb() {
		return bAdverb;
	}

	/**
	 * Returns the OK button.
	 */
	public JButton getBok() {
		return bOk;
	}

	/**
	 * Returns the cancel button.
	 */
	public JButton getBcancel() {
		return bCancel;
	}

	/**
	 * Returns the delete button.
	 */
	public JButton getBdelete() {
		return bDelete;
	}

	//ATTRIBUTES
	/**
	 * Returns the local sentence generator.
	 */
	public SentenceGenerator getLocalStg() {
		return localStg;
	}

	/**
	 * Returns the global sentence generator.
	 */
	public SentenceGenerator getStg() {
		return stg;
	}

	/**
	 * Returns the main window.
	 */
	public MainWindow getMainWindow() {
		return mw;
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		//Set the layout manager
		setLayout(new GridLayout(1,1));
		pContent.setLayout(new GridLayout(6,1,5,5));
		//Add widgets
		add(pContent);
		pStructure.add(tfStructure, BorderLayout.CENTER);
		pStructure.add(bDelete, BorderLayout.EAST);
		pButtons.add(bAdjective);
		pButtons.add(bAdverb);
		pButtons.add(bComp);
		pButtons.add(bCommonName);
		pButtons.add(bProperName);
		pButtons.add(bPrep);
		pButtons.add(bVerb);
		pFooter.add(bOk);
		pFooter.add(bCancel);
		pContent.add(lStructure);
		pContent.add(pStructure);
		pContent.add(pButtons);
		pContent.add(lExample);
		pContent.add(lSentenceExample);
		pContent.add(pFooter);
		//Edit style
		tfStructure.setEnabled(false);
		tfStructure.setDisabledTextColor(Color.BLACK);
		pContent.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
	}

	/**
	 * Adds reactions to widgets.
	 */
	private void addReactions() {
		StructureController ctrl = new StructureController(this);
		bDelete.addActionListener(ctrl);
		bAdjective.addActionListener(ctrl);
		bAdverb.addActionListener(ctrl);
		bComp.addActionListener(ctrl);
		bCommonName.addActionListener(ctrl);
		bProperName.addActionListener(ctrl);
		bPrep.addActionListener(ctrl);
		bVerb.addActionListener(ctrl);
		bOk.addActionListener(ctrl);
		bCancel.addActionListener(ctrl);
	}

	/**
	 * Converts the structure arraylist to an human-readable string, and update the textfield and example label.
	 */
	public void updateReadableStructure() {
		if(localStg.getStructure().size() > 0) {
			String tfStructureText="";
			HashMap<Character,String> corres = new HashMap<Character,String>();
			corres.put('n',"NomCommun");
			corres.put('g',"NomPropre");
			corres.put('v',"Verbe");
			corres.put('p',"Préposition");
			corres.put('a',"Adjectif");
			corres.put('c',"Complément");
			corres.put('d',"Adverbe");
			for(Character c : tmpStructure) {
				tfStructureText += " "+corres.get(c);
			}
			tfStructure.setText(tfStructureText);
			try {
				lSentenceExample.setText("<html>"+localStg.getRandomSentence()+"</html>");
			}
			catch(NoLemmasException e) {
				System.out.println(e.getMessage());
			}
		}
		else {
			tfStructure.setText("");
			lSentenceExample.setText("<html>Ajoutez un type de mot à la structure !</html>");
		}
	}

	/**
	 * Resets the local sentence generator.
	 * Copy the global sentence generator into the temporary sentence generator.
	 */
	public void resetLocalStg() {
		tmpStructure = (ArrayList<Character>) stg.getStructure().clone();
		localStg = new SentenceGenerator(tmpStructure,stg.getVocLists());
	}
}