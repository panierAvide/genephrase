package genephrase.view.window;

import genephrase.controller.window.RandomModeController;
import genephrase.model.lang.SentenceGenerator;
import java.awt.*;
import javax.swing.*;

/**
 * The "random mode" panel, ie the content of the "Mode aléatoire" tab.
 * Allows user to generate totally random sentences.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class RandomModePanel extends JPanel {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	/** The current SentenceGenerator. **/
	private SentenceGenerator stg;
	/** The current MainWindow. **/
	private MainWindow mw;
	/** The default text in the middle. **/
	private String defaultText = "<html>Cliquez sur Générer pour générer une nouvelle phrase !</html>";
	//Widgets
	/** The middle label, which contains the generated sentence. **/
	private JLabel lPhrase = new JLabel(defaultText);
	/** The footer panel, which contains buttons. **/
	private JPanel pButtons = new JPanel(new GridLayout(1,2));
	/** The generate button. **/
	private JButton bGenerer = new JButton("Générer");
	/** The favorite button. **/
	private JButton bFavoris = new JButton("Ajouter aux favoris");

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public RandomModePanel(MainWindow mw, SentenceGenerator stg) {
		super(new BorderLayout());
		this.stg = stg;
		this.mw = mw;
		createInterface();
		addReactions();
	}

//ACCESSORS
	//ATTRIBUTES
	/**
	 * Returns the sentence generator.
	 */
	public SentenceGenerator getSentenceGenerator() {
		return stg;
	}

	/**
	 * Returns the main window.
	 */
	public MainWindow getMainWindow() {
		return mw;
	}

	/**
	 * Returns the default text in the sentence label.
	 */
	public String getDefaultText() {
		return defaultText;
	}

	//WIDGETS
	/**
	 * Returns the "generer" button.
	 */
	public JButton getGenerer() {
		return bGenerer;
	}

	/**
	 * Returns the "favoris" button.
	 */
	public JButton getFavoris() {
		return bFavoris;
	}

	/**
	 * Returns the label which shows the current sentence.
	 */
	public JLabel getPhrase() {
		return lPhrase;
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		//Add objects
		pButtons.add(bGenerer);
		pButtons.add(bFavoris);
		add(lPhrase, BorderLayout.CENTER);
		add(pButtons, BorderLayout.SOUTH);
		//Edit style
		lPhrase.setHorizontalAlignment(SwingConstants.CENTER);
		lPhrase.setFont(new Font("Arial", Font.BOLD, 20));
	}

	/**
	 * Adds reactions to widgets.
	 */
	private void addReactions() {
		RandomModeController ctrl = new RandomModeController(this);
		bGenerer.addActionListener(ctrl);
		bFavoris.addActionListener(ctrl);
	}
}