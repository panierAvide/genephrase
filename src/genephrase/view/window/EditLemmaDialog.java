package genephrase.view.window;

import genephrase.controller.window.EditLemmaController;
import genephrase.model.lang.Lemma;
import java.awt.*;
import javax.swing.*;

/**
 * The edit lemma dialog, to edit or add lemmas.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 * @todo
	Create a new panel, specific for proper names (Radios buttons and choose one form)
 */
public class EditLemmaDialog extends JDialog {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	/** The current DictionnaryPanel. **/
	private DictionnaryPanel dp;
	/** The lemma to edit. **/
	private Lemma currentLemma = null;
	//Widgets
	/** The header panels, which contains pHeaderLabels and pHeaderFields. **/
	private JPanel pHeader = new JPanel(new BorderLayout());
	/** The panel which contains label for header textfields. **/
	private JPanel pHeaderLabels = new JPanel(new GridLayout(3,1));
	/** The panel which contains header textfields. **/
	private JPanel pHeaderFields = new JPanel(new GridLayout(3,1));
	/** The first line of radio buttons. **/
	private JPanel pHeaderRadios1 = new JPanel(new GridLayout(1,4));
	/** The second line of radio buttons. **/
	private JPanel pHeaderRadios2 = new JPanel(new GridLayout(1,4));
	/** The label for lemma textfield. **/
	private JLabel lLemme = new JLabel("Lemme (forme simple d'une famille de mots) :");
	/** The label for word's type textfield. **/
	private JLabel lType = new JLabel("Type de mot :");
	/** The lemma textfield. **/
	private JTextField tfLemme = new JTextField();
	//Radio buttons for type
	/** The type of word radio button group. **/
	private ButtonGroup bgType = new ButtonGroup();
	/** The radio button for proper names. **/
	private JRadioButton rbNomPropre = new JRadioButton("Nom propre");
	/** The radio button for common names. **/
	private JRadioButton rbNomCommun = new JRadioButton("Nom commun");
	/** The radio button for adjectives. **/
	private JRadioButton rbAdjectif = new JRadioButton("Adjectif");
	/** The radio button for adverbs. **/
	private JRadioButton rbAdverbe = new JRadioButton("Adverbe");
	/** The radio button for complements. **/
	private JRadioButton rbComplement = new JRadioButton("Complément");
	/** The radio button for prepositions. **/
	private JRadioButton rbPreposition = new JRadioButton("Préposition");
	/** The radio button for verbs. **/
	private JRadioButton rbVerbe = new JRadioButton("Verbe");
	//Panel for middle content
	/** The middle panel, which contains textfields in function of type. **/
	private JPanel pMiddle = new JPanel(new CardLayout());
	/** The default label for middle panel, shown before a word's type is choosen. **/
	private JLabel lDefault = new JLabel("Choisissez un type de mot");
	//Panel for common names, adjectives
	/** The middle panel for adjectives and names. **/
	private JPanel panelOne = new JPanel(new GridLayout(4,2));
	/** The label for the singular, masculine form. **/
	private JLabel lSingMasc = new JLabel("Singulier, masculin :");
	/** The label for the singular, feminine form. **/
	private JLabel lSingFem = new JLabel("Singulier, féminin :");
	/** The label for the plural, masculine form. **/
	private JLabel lPlurMasc = new JLabel("Pluriel, masculin :");
	/** The label for the plural, feminine form. **/
	private JLabel lPlurFem = new JLabel("Pluriel, féminin :");
	/** The textfield for the singular, masculine form. **/
	private JTextField tfSingMasc = new JTextField();
	/** The textfield for the singular, feminine form. **/
	private JTextField tfSingFem = new JTextField();
	/** The textfield for the plural, masculine form. **/
	private JTextField tfPlurMasc = new JTextField();
	/** The textfield for the plural, feminine form. **/
	private JTextField tfPlurFem = new JTextField();
	//Panel for verbs
	/** The middle panel for verbs. **/
	private JPanel panelTwo = new JPanel(new GridLayout(6,2));
	/** The label for 1st person of singular. **/
	private JLabel lSing1 = new JLabel("1e personne du singulier :");
	/** The label for 2nd person of singular. **/
	private JLabel lSing2 = new JLabel("2e personne du singulier :");
	/** The label for 3rd person of singular. **/
	private JLabel lSing3 = new JLabel("3e personne du singulier :");
	/** The label for 1st person of plural. **/
	private JLabel lPlur1 = new JLabel("1e personne du pluriel :");
	/** The label for 2nd person of plural. **/
	private JLabel lPlur2 = new JLabel("2e personne du pluriel :");
	/** The label for 3rd person of plural. **/
	private JLabel lPlur3 = new JLabel("3e personne du pluriel :");
	/** The textfield for 1st person of singular. **/
	private JTextField tfSing1 = new JTextField();
	/** The textfield for 2nd person of singular. **/
	private JTextField tfSing2 = new JTextField();
	/** The textfield for 3rd person of singular. **/
	private JTextField tfSing3 = new JTextField();
	/** The textfield for 1st person of plural. **/
	private JTextField tfPlur1 = new JTextField();
	/** The textfield for 2nd person of plural. **/
	private JTextField tfPlur2 = new JTextField();
	/** The textfield for 3rd person of plural. **/
	private JTextField tfPlur3 = new JTextField();
	/** The middle label shown when there's no panel corresponding to the current type. **/
	private JLabel lNoPanel = new JLabel("Cliquez sur OK pour ajouter ce lemme.");
	//Footer panel, with buttons
	/** The footer panel, which contains buttons. **/
	private JPanel pFooter = new JPanel(new GridLayout(1,2));
	/** The OK button. **/
	private JButton bOk = new JButton("OK");
	/** The cancel button. **/
	private JButton bCancel = new JButton("Annuler");

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public EditLemmaDialog(DictionnaryPanel dp) {
		super();
		this.dp = dp;
		setTitle("Éditer un lemme");
		setSize(850,300);
		setVisible(false);
		createInterface();
		addReactions();
	}

//ACCESSORS
	//ATTRIBUTES
	/**
	 * Returns the button group of radios.
	 */
	public ButtonGroup getButtonGroup() {
		return bgType;
	}

	/**
	 * Returns the dictionnary panel.
	 */
	public DictionnaryPanel getDictionnaryPanel() {
		return dp;
	}

	/**
	 * Returns the current lemma.
	 * @return The lemma to edit, null if it's a creation
	 */
	public Lemma getCurrentLemma() {
		return currentLemma;
	}

	//WIDGETS
	/**
	 * Returns the OK button.
	 */
	public JButton getBok() {
		return bOk;
	}

	/**
	 * Returns the cancel button.
	 */
	public JButton getBcancel() {
		return bCancel;
	}

	/**
	 * Returns the proper name radio button.
	 */
	public JRadioButton getRbNomPropre() {
		return rbNomPropre;
	}

	/**
	 * Returns the common name radio button.
	 */
	public JRadioButton getRbNomCommun() {
		return rbNomCommun;
	}

	/**
	 * Returns the adjective radio button.
	 */
	public JRadioButton getRbAdjectif() {
		return rbAdjectif;
	}

	/**
	 * Returns the adverb radio button.
	 */
	public JRadioButton getRbAdverbe() {
		return rbAdverbe;
	}

	/**
	 * Returns the complement radio button.
	 */
	public JRadioButton getRbComplement() {
		return rbComplement;
	}

	/**
	 * Returns the preposition radio button.
	 */
	public JRadioButton getRbPreposition() {
		return rbPreposition;
	}

	/**
	 * Returns the verb radio button.
	 */
	public JRadioButton getRbVerbe() {
		return rbVerbe;
	}

	/**
	 * Returns the lemma text field.
	 */
	public JTextField getTfLemma() {
		return tfLemme;
	}

	/**
	 * Returns the 1st person of singular form text field.
	 */
	public JTextField getTfSing1() {
		return tfSing1;
	}

	/**
	 * Returns the 2nd person of singular form text field.
	 */
	public JTextField getTfSing2() {
		return tfSing2;
	}

	/**
	 * Returns the 3rd person of singular form text field.
	 */
	public JTextField getTfSing3() {
		return tfSing3;
	}

	/**
	 * Returns the 1st person of plural form text field.
	 */
	public JTextField getTfPlur1() {
		return tfPlur1;
	}

	/**
	 * Returns the 2nd person of plural form text field.
	 */
	public JTextField getTfPlur2() {
		return tfPlur2;
	}

	/**
	 * Returns the 3rd person of plural form text field.
	 */
	public JTextField getTfPlur3() {
		return tfPlur3;
	}

	/**
	 * Returns the singular, masculine form text field.
	 */
	public JTextField getTfSingMasc() {
		return tfSingMasc;
	}

	/**
	 * Returns the singular, feminine form text field.
	 */
	public JTextField getTfSingFem() {
		return tfSingFem;
	}

	/**
	 * Returns the plural, masculine form text field.
	 */
	public JTextField getTfPlurMasc() {
		return tfPlurMasc;
	}

	/**
	 * Returns the plural, feminine form text field.
	 */
	public JTextField getTfPlurFem() {
		return tfPlurFem;
	}

//OTHER METHODS
	/**
	 * Set up the interface.
	 */
	private void createInterface() {
		//Set layout
		setLayout(new BorderLayout());
		//Set radio buttons
		bgType.add(rbAdjectif);
		bgType.add(rbAdverbe);
		bgType.add(rbComplement);
		bgType.add(rbNomCommun);
		bgType.add(rbNomPropre);
		bgType.add(rbPreposition);
		bgType.add(rbVerbe);
		//Add widgets
		//Header
		pHeader.add(pHeaderLabels, BorderLayout.WEST);
		pHeader.add(pHeaderFields, BorderLayout.CENTER);
		pHeaderLabels.add(lLemme);
		pHeaderLabels.add(lType);
		pHeaderFields.add(tfLemme);
		pHeaderFields.add(pHeaderRadios1);
		pHeaderFields.add(pHeaderRadios2);
		pHeaderRadios1.add(rbAdjectif);
		pHeaderRadios1.add(rbAdverbe);
		pHeaderRadios1.add(rbComplement);
		pHeaderRadios1.add(rbNomCommun);
		pHeaderRadios2.add(rbNomPropre);
		pHeaderRadios2.add(rbPreposition);
		pHeaderRadios2.add(rbVerbe);
		pHeaderRadios2.add(new JPanel());
		//Panel One
		panelOne.add(lSingMasc);
		panelOne.add(lPlurMasc);
		panelOne.add(tfSingMasc);
		panelOne.add(tfPlurMasc);
		panelOne.add(lSingFem);
		panelOne.add(lPlurFem);
		panelOne.add(tfSingFem);
		panelOne.add(tfPlurFem);
		//Panel Two
		panelTwo.add(lSing1);
		panelTwo.add(lPlur1);
		panelTwo.add(tfSing1);
		panelTwo.add(tfPlur1);
		panelTwo.add(lSing2);
		panelTwo.add(lPlur2);
		panelTwo.add(tfSing2);
		panelTwo.add(tfPlur2);
		panelTwo.add(lSing3);
		panelTwo.add(lPlur3);
		panelTwo.add(tfSing3);
		panelTwo.add(tfPlur3);
		//Middle panel
		pMiddle.add(lDefault, "Default");
		pMiddle.add(panelOne, "Panel #1");
		pMiddle.add(panelTwo, "Panel #2");
		pMiddle.add(lNoPanel, "No Panel");
		//Footer
		pFooter.add(bOk);
		pFooter.add(bCancel);
		//Main panel
		add(pHeader, BorderLayout.NORTH);
		//add(lNoPanel, BorderLayout.CENTER);
		add(pMiddle, BorderLayout.CENTER);
		add(pFooter, BorderLayout.SOUTH);
		//Set style
		pHeader.setBorder(BorderFactory.createTitledBorder("Définition du lemme"));
		panelOne.setBorder(BorderFactory.createTitledBorder("Formes"));
		panelTwo.setBorder(BorderFactory.createTitledBorder("Formes"));
	}

	/**
	 * Add reactions to widgets.
	 */
	private void addReactions() {
		EditLemmaController ctrl = new EditLemmaController(this);
		bOk.addActionListener(ctrl);
		bCancel.addActionListener(ctrl);
		rbNomPropre.addActionListener(ctrl);
		rbNomCommun.addActionListener(ctrl);
		rbAdjectif.addActionListener(ctrl);
		rbAdverbe.addActionListener(ctrl);
		rbComplement.addActionListener(ctrl);
		rbPreposition.addActionListener(ctrl);
		rbVerbe.addActionListener(ctrl);
	}
	
	/**
	 * Set the middle content of the dialog.
	 * @param val The wanted content. 0 = no content, 1 = panel for names/adjectives, 2 = panel for verbs
	 */
	public void setMiddleContent(int val) {
		CardLayout cl = (CardLayout) (pMiddle.getLayout());
		if(val==0) {
			cl.show(pMiddle, "Default");
		}
		else if(val==1) {
			cl.show(pMiddle, "Panel #1");
		}
		else if(val==2) {
			cl.show(pMiddle, "Panel #2");
		}
		else if(val==3) {
			cl.show(pMiddle, "No Panel");
		}
	}

	/**
	 * Set the current lemma.
	 * @param lemma The lemma to edit, null to create a new one
	 * @param type The type of this lemma, null to create a new one
	 */
	public void setLemma(Lemma lemma, Character type) {
		if(lemma==null) {
			currentLemma = null;
			//Reset fields
			tfLemme.setText("");
			tfSingMasc.setText("");
			tfSingFem.setText("");
			tfPlurMasc.setText("");
			tfPlurFem.setText("");
			tfSing1.setText("");
			tfSing2.setText("");
			tfSing3.setText("");
			tfPlur1.setText("");
			tfPlur2.setText("");
			tfPlur3.setText("");
			rbNomPropre.setEnabled(true);
			rbNomCommun.setEnabled(true);
			rbAdjectif.setEnabled(true);
			rbAdverbe.setEnabled(true);
			rbComplement.setEnabled(true);
			rbPreposition.setEnabled(true);
			rbVerbe.setEnabled(true);
			bgType.clearSelection();
			setMiddleContent(0);
		}
		else {
			currentLemma = lemma;
			tfLemme.setText(lemma.getLemma());
			//Disable type selection
			rbNomPropre.setEnabled(false);
			rbNomCommun.setEnabled(false);
			rbAdjectif.setEnabled(false);
			rbAdverbe.setEnabled(false);
			rbComplement.setEnabled(false);
			rbPreposition.setEnabled(false);
			rbVerbe.setEnabled(false);
			//Set the current type
			if(type=='n') { rbNomCommun.setSelected(true); }
			else if(type=='g') { rbNomPropre.setSelected(true); }
			else if(type=='a') { rbAdjectif.setSelected(true); }
			else if(type=='d') { rbAdverbe.setSelected(true); }
			else if(type=='c') { rbComplement.setSelected(true); }
			else if(type=='p') { rbPreposition.setSelected(true); }
			else if(type=='v') { rbVerbe.setSelected(true); }
			//Fill text fields
			if(type=='v') {
				try {
					tfSing1.setText(lemma.getForm(Lemma.SINGULAR, 1));
				} catch(NullPointerException e) { tfSing1.setText(""); }
				try {
					tfSing2.setText(lemma.getForm(Lemma.SINGULAR, 2));
				} catch(NullPointerException e) { tfSing2.setText(""); }
				try {
					tfSing3.setText(lemma.getForm(Lemma.SINGULAR, 3));
				} catch(NullPointerException e) { tfSing3.setText(""); }
				try {
					tfPlur1.setText(lemma.getForm(Lemma.PLURAL, 1));
				} catch(NullPointerException e) { tfPlur1.setText(""); }
				try {
					tfPlur2.setText(lemma.getForm(Lemma.PLURAL, 2));
				} catch(NullPointerException e) { tfPlur2.setText(""); }
				try {
					tfPlur3.setText(lemma.getForm(Lemma.PLURAL, 3));
				} catch(NullPointerException e) { tfPlur3.setText(""); }
				setMiddleContent(2);
			}
			else {
				try {
					tfSingMasc.setText(lemma.getForm(Lemma.SINGULAR, Lemma.MASCULINE));
				} catch(NullPointerException e) { tfSingMasc.setText(""); }
				try {
					tfSingFem.setText(lemma.getForm(Lemma.SINGULAR, Lemma.FEMININE));
				} catch(NullPointerException e) { tfSingFem.setText(""); }
				try {
					tfPlurMasc.setText(lemma.getForm(Lemma.PLURAL, Lemma.MASCULINE));
				} catch(NullPointerException e) { tfPlurMasc.setText(""); }
				try {
					tfPlurFem.setText(lemma.getForm(Lemma.PLURAL, Lemma.FEMININE));
				} catch(NullPointerException e) { tfPlurFem.setText(""); }
				if(type=='n' || type=='g' || type=='a') { setMiddleContent(1); }
				else { setMiddleContent(3); }
			}
		}
	}
}