package genephrase.view.window;

import genephrase.model.lang.*;
import genephrase.controller.window.DictionnaryController;
import java.awt.*;
import java.util.HashMap;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * The dictionnary panel, ie the content of the dictionnary tab.
 * Allows user to manage dictionnaries.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class DictionnaryPanel extends JPanel {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	/** The current SentenceGenerator. **/
	private SentenceGenerator stg;
	/** The current MainWindow. **/
	private MainWindow mw;
	/** Correspondance between index in table and a Lemma. **/
	private ArrayList<Lemma> listIndexToLemma = new ArrayList<Lemma>();
	/** Correspondance between index in table and a type of word. **/
	private ArrayList<Character> listIndexToType = new ArrayList<Character>();
	//Widgets
	/** Header label, will contain statistics. **/
	private JLabel lHeader = new JLabel("Dictionnaire");
	/** The table which contains all the lemmas. **/
	private JTable table;
	/** The scroll pane associated to the table. **/
	private JScrollPane spTable;
	/** The table header. **/
	private String[] tableHeader = {"Type","Lemme"};
	/** The content of the table. **/
	private String[][] tableContent;
	/** The panel which contains footer buttons. **/
	private JPanel pButtons = new JPanel(new GridLayout(1,3));
	/** "Add a lemma" button. **/
	private JButton bAdd = new JButton("Ajouter");
	/** "Edit a lemma" button. **/
	private JButton bEdit = new JButton("Éditer");
	/** "Delete a lemma" button. **/
	private JButton bDelete = new JButton("Supprimer");

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public DictionnaryPanel(SentenceGenerator stg, MainWindow mw) {
		super();
		this.stg = stg;
		this.mw = mw;
		createInterface();
		addReactions();
	}

//ACCESSORS
	//ATTRIBUTES
	/**
	 * Returns the list "table index to lemma".
	 */
	public ArrayList<Lemma> getListIndexToLemma() {
		return listIndexToLemma;
	}

	/**
	 * Returns the list "table index to type".
	 */
	public ArrayList<Character> getListIndexToType() {
		return listIndexToType;
	}

	/**
	 * Returns the main window.
	 */
	public MainWindow getMainWindow() {
		return mw;
	}

	/**
	 * Returns the sentence Generator.
	 */
	public SentenceGenerator getStg() {
		return stg;
	}

	//WIDGETS
	/**
	 * Returns the add button.
	 */
	public JButton getBadd() {
		return bAdd;
	}

	/**
	 * Returns the edit button.
	 */
	public JButton getBedit() {
		return bEdit;
	}

	/**
	 * Returns the delete button.
	 */
	public JButton getBdelete() {
		return bDelete;
	}

	/**
	 * Returns the JTable.
	 */
	public JTable getTable() {
		return table;
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		table = new JTable();
		spTable = new JScrollPane(table);
		initTableContent();
		//Set the layout manager
		setLayout(new BorderLayout());
		//Add widgets
		pButtons.add(bAdd);
		pButtons.add(bEdit);
		pButtons.add(bDelete);
		add(lHeader, BorderLayout.NORTH);
		add(spTable, BorderLayout.CENTER);
		add(pButtons, BorderLayout.SOUTH);
		//Edit style
		bEdit.setEnabled(false);
		bDelete.setEnabled(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	/**
	 * Adds reactions to widgets.
	 */
	private void addReactions() {
		DictionnaryController ctrl = new DictionnaryController(this);
		bAdd.addActionListener(ctrl);
		bEdit.addActionListener(ctrl);
		bDelete.addActionListener(ctrl);
		table.addMouseListener(ctrl);
		table.getSelectionModel().addListSelectionListener(ctrl);
	}

	/**
	 * Initializes the table content.
	 */
	public void initTableContent() {
		mw.progress.setLocationRelativeTo(this);
		mw.progress.setVisible(true);
		table.setModel(new DefaultTableModel());
		HashMap<Character,Dictionnary> vocLists = stg.getVocLists();
		//Get the quantity of lemmas
		int nbLemmes = 0;
		for(Character c : vocLists.keySet()) {
			nbLemmes += vocLists.get(c).getWords().size();
		}
		updateStatistics();
		if(nbLemmes == 0) {
			lHeader.setText("<html>Le dictionnaire est vide. Ajoutez un mot en cliquant sur le bouton ci-dessous, ou lancez une analyse de dictionnaire XML (Fichier > Charger un dictionnaire).</html>");
		}
		tableContent = new String[nbLemmes][2];
		listIndexToLemma.ensureCapacity(nbLemmes);
		//Fill the table
		HashMap<Character,String> corres = new HashMap<Character,String>();
		corres.put('n',"Nom commun");
		corres.put('g',"Nom propre");
		corres.put('v',"Verbe");
		corres.put('p',"Préposition");
		corres.put('a',"Adjectif");
		corres.put('c',"Complément");
		corres.put('d',"Adverbe");
		int index=0;
		for(Character c : vocLists.keySet()) {
			ArrayList<Lemma> lemmas = vocLists.get(c).getWords();
			for(Lemma l : lemmas) {
				tableContent[index][0] = corres.get(c);
				tableContent[index][1] = l.getLemma();
				listIndexToLemma.add(index, l);
				listIndexToType.add(index, c);
				index++;
			}
		}
		//Edit the JTable
		table.setModel(new DefaultTableModel(tableContent, tableHeader) {
			public boolean isCellEditable(int rowIndex, int colIndex) {
				return false;
			}
		});
		mw.progress.setVisible(false);
	}

	/**
	 * Updates only statistics.
	 */
	public void updateStatistics() {
		HashMap<Character,Dictionnary> vocLists = stg.getVocLists();
		long possibilities = 1L; //Set to 1 because we multiply by previous number, if 0 we multiply each time by 0 !
		for(Character c : stg.getStructure()) {
			long dicoSize = (long) vocLists.get(c).getWords().size();
			if(dicoSize > 0) { possibilities = possibilities * dicoSize; }
		}
		//If we return to negatives, we had more than the limit of longs !
		String result = null;
		if(possibilities < 0L) { result = "<html>Statistiques : + de "+Long.MAX_VALUE+" combinaisons possibles avec cette structure !</html>"; }
		else { result = "<html>Statistiques : "+possibilities+" combinaisons possibles avec cette structure.</html>"; }
		lHeader.setText(result);
	}
}