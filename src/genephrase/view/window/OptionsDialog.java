package genephrase.view.window;

import genephrase.controller.window.OptionsController;
import genephrase.model.Options;
import java.awt.*;
import javax.swing.*;

/**
 * The options dialog, to set parameters of the application.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class OptionsDialog extends JDialog {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	//Widgets
	/** The panel which contains all options groups (edit this to add a new option panel. */
	private JPanel pContent = new JPanel(new GridLayout(1,1));
	//Max lemmas option
	/** The panel which contains "maxi of lemmas" option. **/
	private JPanel pLemmas = new JPanel(new GridLayout(3,1));
	/** The radio button for auto mode. **/
	private JRadioButton rbLemmasAuto = new JRadioButton("Automatique, l'application détermine le nombre de lemmes à charger.");
	/** The radio button for maxi mode. **/
	private JRadioButton rbLemmasMaxi = new JRadioButton("Maximum, charger le dictionnaire complet.");
	/** The radio button for fixed value mode. **/
	private JRadioButton rbLemmasFixe = new JRadioButton("Fixé, choisissez le nombre de lemmes à charger :");
	/** The radio button group for lemmas option. **/
	private ButtonGroup rbLemmas = new ButtonGroup();
	/** The spinner to choose a fixed value for max lemmas option. **/
	private JSpinner spLemmas = new JSpinner(new SpinnerNumberModel(100, 1, 500000, 1));
	/** The panel which contains radio button and spinner for "fixed value" parameter for "max lemmas" option. **/
	private JPanel pLemmasFixe = new JPanel(new GridLayout(1,2));
	//Button panel
	/** The panel which contains footer buttons. **/
	private JPanel pButtons = new JPanel(new GridLayout(1,2));
	/** The OK button. **/
	private JButton bOk = new JButton("OK");
	/** The cancel button. **/
	private JButton bCancel = new JButton("Annuler");

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public OptionsDialog() {
		super();
		setTitle("Préférences");
		setSize(600,200);
		setVisible(false);
		createInterface();
		updateFields();
		addReactions();
	}

//ACCESSORS
	//WIDGETS
	/**
	 * Returns the "auto lemmas" radio button.
	 */
	public JRadioButton getLemmasAuto() {
		return rbLemmasAuto;
	}

	/**
	 * Returns the "maxi lemmas" radio button.
	 */
	public JRadioButton getLemmasMaxi() {
		return rbLemmasMaxi;
	}

	/**
	 * Returns the "fixe lemmas" radio button.
	 */
	public JRadioButton getLemmasFixe() {
		return rbLemmasFixe;
	}

	/**
	 * Returns the OK button.
	 */
	public JButton getOk() {
		return bOk;
	}

	/**
	 * Returns the cancel button.
	 */
	public JButton getCancel() {
		return bCancel;
	}

	/**
	 * Returns the lemmas spinner.
	 */
	public JSpinner getSpinner() {
		return spLemmas;
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		//Set the layout manager
		setLayout(new BorderLayout());
		//init options
		initLemmas();
		//Add widgets
		pContent.add(pLemmas);
		pButtons.add(bOk);
		pButtons.add(bCancel);
		add(pContent, BorderLayout.CENTER);
		add(pButtons, BorderLayout.SOUTH);
	}

	/**
	 * Adds reactions to widgets.
	 */
	private void addReactions() {
		OptionsController ctrl = new OptionsController(this);
		rbLemmasAuto.addActionListener(ctrl);
		rbLemmasFixe.addActionListener(ctrl);
		rbLemmasMaxi.addActionListener(ctrl);
		bOk.addActionListener(ctrl);
		bCancel.addActionListener(ctrl);
	}

	/**
	 * Inits the lemma parameter.
	 */
	private void initLemmas() {
		rbLemmas.add(rbLemmasAuto);
		rbLemmas.add(rbLemmasMaxi);
		rbLemmas.add(rbLemmasFixe);
		//Widgets
		pLemmasFixe.add(rbLemmasFixe);
		pLemmasFixe.add(spLemmas);
		pLemmas.add(rbLemmasAuto);
		pLemmas.add(rbLemmasMaxi);
		pLemmas.add(pLemmasFixe);
		//Edit style
		pLemmas.setBorder(BorderFactory.createTitledBorder("Nombre de lemmes maximum au chargement d'un dictionnaire"));
		spLemmas.setEnabled(false);
	}

	/**
	 * Updates fields.
	 */
	public void updateFields() {
		//Select the current lemmas parameter
		if(Options.maxLemmasLoad==0) {
			rbLemmasAuto.setSelected(true);
			rbLemmasFixe.setSelected(false);
			rbLemmasMaxi.setSelected(false);
			spLemmas.setEnabled(false);
		}
		else if(Options.maxLemmasLoad==-1) {
			rbLemmasMaxi.setSelected(true);
			rbLemmasAuto.setSelected(false);
			rbLemmasFixe.setSelected(false);
			spLemmas.setEnabled(false);
		}
		else {
			rbLemmasAuto.setSelected(false);
			rbLemmasMaxi.setSelected(false);
			rbLemmasFixe.setSelected(true);
			spLemmas.setEnabled(true);
			spLemmas.setValue(Options.maxLemmasLoad);
		}
	}
}