package genephrase.view.window;

import genephrase.controller.Genephrase;
import genephrase.controller.window.AboutController;
import java.awt.*;
import java.net.URL;
import java.net.URLClassLoader;
import javax.swing.*;

/**
 * The "about" dialog, to know more about the application.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class AboutDialog extends JDialog {
//ATTRIBUTES
	/** Version of the class (for serialization). **/
	private static final long serialVersionUID = 1L;
	//Widgets
	/** Logo of the application. **/
	private ImageIcon icon;
	/** Label containing name of the application and logo. **/
	private JLabel lTitle;
	/** Label containing informations about application. **/
	private JLabel lAbout = new JLabel("<html><center>Générateur de phrases aléatoires<br />Version "+Genephrase.VERSION+"<br /><br />Auteur : Adrien Pavie<br /><br />(c) 2012 Adrien Pavie<br />Distribué sous licence GPL</center></html>", JLabel.CENTER);
	/** Ok button, to close the dialog. **/
	private JButton bOk = new JButton("OK");

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public AboutDialog() {
		super();
		setTitle("À propos de Généphrase");
		setSize(300,320);
		setVisible(false);
		createInterface();
		addReactions();
	}

//ACCESSORS
	//WIDGETS
	/**
	 * Returns the OK button.
	 */
	public JButton getBok() {
		return bOk;
	}

//OTHER METHODS
	/**
	 * Set up the interface.
	 */
	private void createInterface() {
		//Find the icon (first in jar, if errrors in folders)
		try {
			icon = new ImageIcon(getClass().getClassLoader().getResource("data/gLogo.png"));
		}
		catch(Exception e) {
			System.out.println("Logo introuvable dans le Jar, recherche dans les dossiers.");
			icon = new ImageIcon("data/gLogo.png");
		}
		lTitle = new JLabel("Généphrase", icon, JLabel.CENTER);
		//Set the layout manager
		setLayout(new BorderLayout());
		//Add widgets
		add(lTitle, BorderLayout.NORTH);
		add(lAbout, BorderLayout.CENTER);
		add(bOk, BorderLayout.SOUTH);
		//Edit style
		lTitle.setVerticalTextPosition(JLabel.BOTTOM);
		lTitle.setHorizontalTextPosition(JLabel.CENTER);
		lTitle.setFont(new Font("Arial", Font.BOLD, 20));
		lTitle.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
	}

	/**
	 * Add reactions to widgets.
	 */
	private void addReactions() {
		AboutController ctrl = new AboutController(this);
		bOk.addActionListener(ctrl);
	}
}