package genephrase.view.window;

import genephrase.controller.window.FavoritesController;
import genephrase.model.Options;
import java.awt.*;
import javax.swing.*;

/**
 * The favorite sentences panel.
 * Allows user to see and manage his favorites sentences.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class FavoritesPanel extends JPanel {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	//Widgets
	/** Header label. **/
	private JLabel lHeader = new JLabel("Vos phrases favorites");
	/** The list of favorites sentences. **/
	private JList liste;
	/** The scroll pane associated to the list. **/
	private JScrollPane scrListe;
	/** The delete button. **/
	private JButton bDelete = new JButton("Supprimer");

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public FavoritesPanel() {
		super();
		createInterface();
		addReactions();
	}

//ACCESSORS
	//WIDGETS
	/**
	 * Returns the delete button.
	 */
	public JButton getBdelete() {
		return bDelete;
	}

	/**
	 * Returns the JList.
	 */
	public JList getListe() {
		return liste;
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		//Init objects
		liste = new JList( Options.favorites.toArray(new String[Options.favorites.size()]) );
		scrListe = new JScrollPane(liste);
		//Set the layout manager
		setLayout(new BorderLayout());
		//Add widgets
		add(lHeader, BorderLayout.NORTH);
		add(scrListe, BorderLayout.CENTER);
		add(bDelete, BorderLayout.SOUTH);
		//Edit style
		bDelete.setEnabled(false);
		liste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	/**
	 * Adds reactions to widgets.
	 */
	private void addReactions() {
		FavoritesController ctrl = new FavoritesController(this);
		bDelete.addActionListener(ctrl);
		liste.addListSelectionListener(ctrl);
	}

	/**
	 * Updates the list of favorites.
	 */
	public void updateFavorites() {
		liste.setListData( Options.favorites.toArray(new String[Options.favorites.size()]) );
	}
}