package genephrase.view.window;

import java.awt.*;
import javax.swing.*;

/**
 * The progress dialog, to show progress of a task.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class ProgressDialog extends JDialog {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	//Widgets
	/** The panel which contains the content of the dialog (to set up a border). **/
	private JPanel pContent = new JPanel(new BorderLayout(10,10));
	/** The "please be patient" label. **/
	private JLabel lPatient = new JLabel("Veuillez patienter...");
	/** The progress bar. **/
	private JProgressBar pg = new JProgressBar();

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public ProgressDialog() {
		super();
		setTitle("Exécution en cours...");
		setSize(300,100);
		setVisible(false);
		createInterface();
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		//Set the layout manager
		setLayout(new GridLayout(1,1));
		//Add widgets
		add(pContent);
		pContent.add(lPatient, BorderLayout.NORTH);
		pContent.add(pg, BorderLayout.CENTER);
		//Edit style
		pg.setIndeterminate(true);
		pContent.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
	}
}