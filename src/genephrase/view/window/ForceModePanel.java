package genephrase.view.window;

import genephrase.controller.window.ForceModeController;
import genephrase.model.lang.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.*;

/**
 * The "force mode" panel, ie the content of the "Mode forcé" tab.
 * Allows user to generate sentence where some parts are defined by the user.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class ForceModePanel extends JPanel {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	/** The current SentenceGenerator. **/
	private SentenceGenerator stg;
	/** The current MainWindow. **/
	private MainWindow mw;
	/** The default text, shown in the middle. **/
	private String defaultText = "<html>Cliquez sur Générer pour générer une nouvelle phrase !</html>";
	//Widgets
	/** The footer panel, which contains buttons and parameters. **/
	private JPanel pFooter = new JPanel(new BorderLayout());
	/** The parameter panel, where user can define forced names. **/
	private JPanel pParam = new JPanel();
	/** The buttons panel. **/
	private JPanel pButtons = new JPanel(new GridLayout(1,2));
	/** The middle label, which show the generated sentence. **/
	private JLabel lPhrase = new JLabel(defaultText);
	/** The parameter label. **/
	private JLabel lParam = new JLabel("<html>Pour forcer un nom à apparaître, remplissez le champ correspondant. Il doit être à la 3e personne du singulier (ex : \"Toto\"). Si vous souhaitez laisser le hasard faire, laissez tout simplement le champ vide.</html>");
	/** The generate button. **/
	private JButton bGenerer = new JButton("Générer");
	/** The favorite button. **/
	private JButton bFavoris = new JButton("Ajouter aux favoris");
	/** The list of forced proper names textfields. **/
	private ArrayList<JTextField> tfWord = new ArrayList<JTextField>();

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public ForceModePanel(SentenceGenerator stg, MainWindow mw) {
		super(new BorderLayout());
		this.stg = stg;
		this.mw = mw;
		createInterface();
		addReactions();
	}

//ACCESSORS
	//ATTRIBUTES
	/**
	 * Returns the sentence generator.
	 */
	public SentenceGenerator getSentenceGenerator() {
		return stg;
	}

	/**
	 * Returns the main window.
	 */
	public MainWindow getMainWindow() {
		return mw;
	}

	/**
	 * Returns the default text in the sentence label.
	 */
	public String getDefaultText() {
		return defaultText;
	}

	//WIDGETS
	/**
	 * Returns the "generer" button.
	 */
	public JButton getGenerer() {
		return bGenerer;
	}

	/**
	 * Returns the "favoris" button.
	 */
	public JButton getFavoris() {
		return bFavoris;
	}

	/**
	 * Returns the label which shows the current sentence.
	 */
	public JLabel getPhrase() {
		return lPhrase;
	}

	/**
	 * Returns the list of textfields.
	 */
	public ArrayList<JTextField> getTfWord() {
		return tfWord;
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		//pParam
		pParam.setLayout(new BoxLayout(pParam, BoxLayout.PAGE_AXIS));
		updateTfs();
		//pButtons
		pButtons.add(bGenerer);
		pButtons.add(bFavoris);
		//pFooter
		pFooter.add(pParam, BorderLayout.CENTER);
		pFooter.add(pButtons, BorderLayout.SOUTH);
		//Main panel
		add(lPhrase, BorderLayout.CENTER);
		add(pFooter, BorderLayout.SOUTH);
		//add(bGenerer, BorderLayout.SOUTH);
		//Edit style
		pParam.setBorder(BorderFactory.createTitledBorder("Paramètres"));
		lPhrase.setHorizontalAlignment(SwingConstants.CENTER);
		lPhrase.setFont(new Font("Arial", Font.BOLD, 20));
	}

	/**
	 * Adds reactions to widgets.
	 */
	private void addReactions() {
		ForceModeController ctrl = new ForceModeController(this);
		bGenerer.addActionListener(ctrl);
		bFavoris.addActionListener(ctrl);
	}

	/**
	 * Updates the textfields.
	 */
	public void updateTfs() {
		pParam.removeAll();
		tfWord.clear();
		//Force names, create the textfields
		boolean hasProperNames = false;
		for(char type : stg.getStructure()) {
			if(type=='g') {
				tfWord.add(new JTextField());
				hasProperNames = true;
			}
		}
		//Add objects
		if(hasProperNames) {
			int indexName=1;
			pParam.add(lParam);
			for(JTextField tf : tfWord) {
				pParam.add(new JLabel("Nom propre n°"+indexName+" :"));
				pParam.add(tf);
				indexName++;
			}
		}
		else { pParam.add(new JLabel("Pas de noms propres dans la structure de phrase, vous ne pouvez pas forcer.")); }
		pParam.validate();
	}
}