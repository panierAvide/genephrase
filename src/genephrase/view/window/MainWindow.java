package genephrase.view.window;

import genephrase.controller.window.MainController;
import genephrase.model.lang.SentenceGenerator;
import java.awt.*;
import java.awt.event.WindowEvent;
import javax.swing.*;

/**
 * The main view of the application.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class MainWindow extends JFrame {
//ATTRIBUTES
	/** Class version (for serialization). **/
	private static final long serialVersionUID = 1L;
	/** The current SentenceGenerator. **/
	private SentenceGenerator stg;
	//Widgets
	/** The menu bar. **/
	private JMenuBar menuBar = new JMenuBar();
	/** The file menu. **/
	private JMenu menuFichier = new JMenu("Fichier");
	/** The tools menu. **/
	private JMenu menuOutils = new JMenu("Outils");
	/** The help menu. **/
	private JMenu menuAide = new JMenu("Aide");
	//File menu
	/** The "load dictionnary" menu in file menu. **/
	private JMenuItem menuFileLoadDico = new JMenuItem("Charger un dictionnaire");
	/** The quit menu in file menu. **/
	private JMenuItem menuFileQuit = new JMenuItem("Quitter");
	//Tools menu
	/** The "edit sentence structure" menu in tools menu. **/
	private JMenuItem menuOutilsStruct = new JMenuItem("Éditer la structure des phrases");
	/** The preferences menu in tools menu. **/
	private JMenuItem menuOutilsPref = new JMenuItem("Préférences");
	//Help menu
	/** The "about" menu in help menu. **/
	private JMenuItem menuAideApropos = new JMenuItem("À propos");
	//TabbedPane
	/** The tabbed pane. **/
	private JTabbedPane tabs = new JTabbedPane();
	/** The current RandomModePanel. **/
	private RandomModePanel randomPane;
	/** The current ForceModePanel. **/
	private ForceModePanel forcePane;
	/** The current DictionnaryPanel. **/
	private DictionnaryPanel dicoPane;
	/** The current FavoritesPanel. **/
	private FavoritesPanel favoritesPane = new FavoritesPanel();
	//Dialogs
	/** The current StructureDialog. **/
	private StructureDialog dStructure;
	/** The current AboutDialog. **/
	private AboutDialog dAbout = new AboutDialog();
	/** The current OptionsDialog. **/
	private OptionsDialog dOptions = new OptionsDialog();
	/** The current EditLemmaDialog. **/
	private EditLemmaDialog dLemma;
	/** The file chooser, to load a dictionnary. **/
	private JFileChooser fc = new JFileChooser();
	/** The progress dialog. **/
	public ProgressDialog progress = new ProgressDialog();

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public MainWindow(SentenceGenerator stg) {
		super("Généphrase");
		this.stg = stg;
		createInterface();
		addReactions();
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setSize(800,600);
		this.setVisible(true);
	}

//ACCESSORS
	//WIDGETS
	/**
	 * Returns the "Load dictionnary" MenuItem.
	 */
	public JMenuItem getLoadDictionnary() {
		return menuFileLoadDico;
	}

	/**
	 * Returns the "Quit" MenuItem.
	 */
	public JMenuItem getQuit() {
		return menuFileQuit;
	}

	/**
	 * Returns the "Edit the structure" MenuItem.
	 */
	public JMenuItem getEditStructure() {
		return menuOutilsStruct;
	}

	/**
	 * Returns the "Preferences" MenuItem.
	 */
	public JMenuItem getPreferences() {
		return menuOutilsPref;
	}

	/**
	 * Returns the "About" MenuItem.
	 */
	public JMenuItem getApropos() {
		return menuAideApropos;
	}

	//PANELS
	/**
	 * Returns the dictionnary panel.
	 */
	public DictionnaryPanel getDictionnaryPanel() {
		return dicoPane;
	}

	/**
	 * Returns the favorites panel.
	 */
	public FavoritesPanel getFavoritesPanel() {
		return favoritesPane;
	}

	//DIALOGS
	/**
	 * Returns the "About" dialog.
	 */
	public AboutDialog getAboutDialog() {
		return dAbout;
	}

	/**
	 * Returns the structure dialog.
	 */
	public StructureDialog getStructureDialog() {
		return dStructure;
	}

	/**
	 * Returns the options dialog.
	 */
	public OptionsDialog getOptionsDialog() {
		return dOptions;
	}

	/**
	 * Returns the "edit lemma" dialog.
	 */
	public EditLemmaDialog getEditLemmaDialog() {
		return dLemma;
	}

	/**
	 * Returns the file chooser dialog.
	 */
	public JFileChooser getFileChooser() {
		return fc;
	}

	/**
	 * Returns the force mode panel.
	 */
	public ForceModePanel getForceModePanel() {
		return forcePane;
	}

//OTHER METHODS
	/**
	 * Sets up the interface.
	 */
	private void createInterface() {
		//Create objects
		randomPane = new RandomModePanel(this, stg);
		forcePane = new ForceModePanel(stg, this);
		dicoPane  = new DictionnaryPanel(stg, this);
		dStructure = new StructureDialog(stg, this);
		dLemma = new EditLemmaDialog(dicoPane);
		//Set the layout manager
		this.setLayout(new BorderLayout());
		//Set menus
		menuBar.add(menuFichier);
		menuBar.add(menuOutils);
		menuBar.add(menuAide);
		menuFichier.add(menuFileLoadDico);
		menuFichier.addSeparator();
		menuFichier.add(menuFileQuit);
		menuOutils.add(menuOutilsStruct);
		menuOutils.addSeparator();
		menuOutils.add(menuOutilsPref);
		menuAide.add(menuAideApropos);
		//Set tabbed pane
		tabs.addTab("Mode aléatoire", randomPane);
		tabs.addTab("Mode forcé", forcePane);
		tabs.addTab("Phrases favorites", favoritesPane);
		tabs.addTab("Dictionnaire", dicoPane);
		//Add widgets to window
		add(menuBar, BorderLayout.NORTH);
		add(tabs, BorderLayout.CENTER);
	}

	/**
	 * Adds reactions to widgets.
	 */
	private void addReactions() {
		MainController ctrl = new MainController(this, stg);
		menuFileLoadDico.addActionListener(ctrl);
		menuFileQuit.addActionListener(ctrl);
		menuOutilsPref.addActionListener(ctrl);
		menuOutilsStruct.addActionListener(ctrl);
		menuAideApropos.addActionListener(ctrl);
		//Window events
		this.addWindowListener(ctrl);
	}

	/**
	 * Closes really the application.
	 */
	public void quit() {
		setVisible(false);
		System.exit(0);
	}

	/**
	 * Launches close signal.
	 */
	public void close() {
		processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
}