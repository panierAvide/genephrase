package genephrase.view.console;

import genephrase.controller.Genephrase;
import java.util.ArrayList;

/**
 * Contains common texts.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Class creation
 */
public class CommonView {
//ATTRIBUTES
	/** Application's header string. **/
	public static final String HEADER = "Généphrase - Générateur de phrases aléatoires\n Version "+Genephrase.VERSION;

//OTHER METHODS
	/**
	 * Prints a list of strings.
	 * @param list The ArrayList of strings to print
	 * @param start The start of the line (to insert tabulations, notes...)
	 */
	public static void printList(ArrayList<String> list, String start) {
		for(String str : list) {
			System.out.println(start+" "+str);
		}
	}

	/**
	 * Prints text into a new line.
	 * Used to distinct controllers and view, although it's just a System.out.println.
	 * @param str The string to print
	 */
	public static void println(String str) {
		System.out.println(str);
	}

	/**
	 * Prints text.
	 * Used to distinct controllers and view, although it's just a System.out.print.
	 * @param str The string to print
	 */
	public static void print(String str) {
		System.out.print(str);
	}
}