package genephrase.view.console;

/**
 * Shows help to user.
 * Prints main commands and parameters.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Class creation
 */
public class HelpView {
//ATTRIBUTES
	/** Help about command structure. **/
	private static final String USAGE = "Usage: genephrase [gui] [options]";
	/** Help about [gui] parameter in command. **/
	private static final String GUI = "Paramètre [gui]:\n\tDéfinit l'interface à utiliser pour l'application.\n\tconsole: démarrer en console (terminal)\n\twindow: lancer le mode fenêtre";
	/** Help about [options] parameters in command. **/
	private static final String OPTIONS = "Options:\n\t--help\t\t\t\t\tAfficher ce texte d'aide\n\t--load <file>\t\t\t\tCharger un dictionnaire depuis le disque dur\n\t--structure <Suite de caractères>\tDéfinir la structure de phrase (voir ci-dessous)\n\t--silent\t\t\t\tCacher les avertissements, erreurs \n\t--force <nom1>, <nom2>...\t\tForcer les noms propres (x pour laisser un nom au hasard)";
	/** Help about sentence structure (option --structure). **/
	private static final String STRUCTURE = "Structure:\n\tLa structure de la phrase définit comment les mots doivent se succèder dans la phrase générée.\n  Types de mots:\n\ta: Adjectif\n\td: Adverbe\n\tc: Complement\n\tn: Nom commun\n\tg: Nom propre\n\tp: Préposition\n\tv: Verbe\n  Exemple:\n\tnvg : NomCommun Verbe NomPropre";

//CONSTRUCTOR
	/**
	 * Class constructor.
	 * Calls a private method, who prints help.
	 */
	public HelpView() {
		printHelp();
	}

//OTHER METHODS
	/**
	 * Prints help.
	 * Shows commands, parameters.
	 */
	private void printHelp() {
		CommonView.println("\n"+USAGE+"\n\n"+GUI+"\n\n"+OPTIONS+"\n\n"+STRUCTURE);
	}
}