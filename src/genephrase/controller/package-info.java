/**
 * Application controller's main package.
 * Controller links model and view in the MVC model.
 */
package genephrase.controller;