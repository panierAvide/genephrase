package genephrase.controller;

import genephrase.controller.console.MainController;
import genephrase.view.console.*;
import genephrase.view.window.MainWindow;
import genephrase.model.Options;
import genephrase.model.lang.SentenceGenerator;
import java.io.File;
import java.util.ArrayList;

/**
 * Launcher for the application.
 * Allows you to choose the application GUI (console or window) and starts it.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 * @todo
	Localization (english first)
 */
public class Genephrase {
//ATTRIBUTES
	/** The current application version. Format: X.x (Month Year) **/
	public static final String VERSION = "1.0 (Août 2012)";
	/** The current MainWindow. **/
	private MainWindow wMain;
	/** The current SentenceGenerator. **/
	private SentenceGenerator stg;
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param args The application arguments (unmodified).
	 */
	public Genephrase(String[] args) {
		//Init folder if needed
		File folder = new File(Options.FOLDER);
		if(!folder.exists()) { folder.mkdirs(); }
		//The first string in the array defines the GUI (console, window, or help if no parameter).
		if(args.length==0 || args[0].equals("")) {
			launchWindow();
		}
		else if(args[0].equals("console")) {
			//Create default structure
			ArrayList<Character> defStruct = new ArrayList<Character>();
			defStruct.add('n');
			defStruct.add('a');
			defStruct.add('v');
			defStruct.add('d');
			defStruct.add('g');
			stg = new SentenceGenerator(defStruct);
			MainController mc = new MainController(stg, args);
		}
		else if(args[0].equals("window")) {
			launchWindow();
		}
		else {
			CommonView.println(CommonView.HEADER);
			HelpView hv = new HelpView();
		}
	}

//OTHER METHODS
	/**
	 * Application launcher.
	 * @param args A list of options
	 */
	public static void main(String[] args) {
		Genephrase app = new Genephrase(args);
	}

	/**
	 * Launches Genephrase in window mode.
	 */
	private void launchWindow() {
		//Create default structure
		ArrayList<Character> defStruct = new ArrayList<Character>();
		defStruct.add('n');
		defStruct.add('a');
		defStruct.add('v');
		defStruct.add('d');
		defStruct.add('g');
		Options.load();
		stg = new SentenceGenerator(defStruct);
		stg.loadDictionnaries(Options.FOLDER+"dico.bin");
		wMain = new MainWindow(stg);
	}
}