/**
 * Console controller's package.
 * Controller associated to the console view.
 */
package genephrase.controller.console;