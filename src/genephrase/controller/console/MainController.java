package genephrase.controller.console;

import genephrase.model.*;
import genephrase.model.lang.SentenceGenerator;
import genephrase.view.console.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Main controller for console interface.
 * Analyzes user commands and reacts (launch sentence generation, launch dictionnaries loading...)
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Class creation
 */
public class MainController {
//ATTRIBUTES
	/** Determines if the application has to be run in silent mode. In silent mode, only the generated sentence is shown. **/
	private boolean silent = false;
	/** Determines if the application has to be run in force mode. In force mode, user chooses proper names. **/
	private boolean forceMode = false;
	/** Determines if the application has to load a XML dictionnary first. **/
	private boolean loadDico = false;
	/** Determines if the application has to use an user-defined sentence structure. **/
	private boolean structureDef = false;
	/** Determines if the application has to show help. **/
	private boolean help = false;
	/** The list of proper names defined by user. **/
	private ArrayList<String> forceNames = new ArrayList<String>();
	/** The sentence structure the application has to use. **/
	private ArrayList<Character> structure = new ArrayList<Character>();
	/** The list of application warnings during runtime. **/
	private ArrayList<String> averts = new ArrayList<String>();
	/** The filename of the XML dictionnary to load. **/
	private String fileDico;

//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param stg The current sentence generator
	 * @param args The application arguments (unmodified)
	 */
	public MainController(SentenceGenerator stg, String[] args) {
		analyzeOptions(args);
		//Print header and warnings
		if(!silent) {
			CommonView.println(CommonView.HEADER);
			if(averts.size() > 0) {
				CommonView.println("\nAvertissements:");
				CommonView.printList(averts, "  ");
				averts.clear();
			}
		}
		//Show help
		if(help) {
			HelpView hw = new HelpView();
		}
		//Or start a task
		else {
			//Analyze a XML dictionnary
			if(loadDico) {
				File file = new File(fileDico);
				Thread th = null;
				try {
					if(!silent) {
						CommonView.println("");
					}
					stg.setVocLists(XmlParser.readFile(file, 0));
					stg.saveDictionnaries(Options.FOLDER+"dico.bin");
				}
				catch(NoLemmasException err1) {
					averts.add("Aucun lemme trouvé dans ce fichier !");
				}
				catch(FileNotFoundException err2) {
					averts.add("Fichier non trouvé !");
				}
				finally {
					if(!silent) {
						if(averts.size()>0) {
							CommonView.printList(averts, "  ");
							averts.clear();
						}
					}
				}
			}
			//Else, load saved dictionnaries
			else {
				stg.loadDictionnaries(Options.FOLDER+"dico.bin");
			}
			//Change the structure
			if(structureDef) {
				stg.setStructure(structure);
			}
			//Force mode
			if(forceMode) {
				try {
					String result = stg.getRandomSentence(forceNames);
					CommonView.println(result);
				}
				catch(NoLemmasException err) {
					if(!silent) {
						CommonView.println("Une liste de mots est vide, vous ne pouvez pas générer de phrase avec cette structure.\nPour remédier à ce problème, chargez un dictionnaire plus complet.");
					}
				}
			}
			//Or random mode
			else {
				try {
					String result = stg.getRandomSentence();
					CommonView.println(result);
				}
				catch(NoLemmasException err) {
					if(!silent) {
						CommonView.println("Une liste de mots est vide, vous ne pouvez pas générer de phrase avec cette structure.\nPour remédier à ce problème, chargez un dictionnaire plus complet.");
					}
				}
			}
		}
	}

//OTHER METHODS
	/**
	 * Analyzes the application options, and edits attributes.
	 * @param args The application arguments (unmodified)
	 */
	private void analyzeOptions(String[] args) {
		//First, we merge all the string into one string
		String command="";
		for(String str : args) {
			command += " "+str;
		}
		//Then, we split this command into multiple options, delimiter : " --"
		command = command.substring(8);
		String[] options = command.split("--");
		//And we analyze each option
		for(String opt : options) {
			//Silent mode
			if(opt.trim().equals("silent")) { silent = true; }
			//Force mode
			else if(opt.startsWith("force ")) {
				opt = opt.substring(6); //Delete "force"
				//Get each name
				String[] names = opt.split(",");
				int nbNullNames = 0;
				for(String n : names) {
					n = n.trim();
					//Test the firstChar before, in case of no char defined
					char firstChar='x';
					try { firstChar = n.charAt(0); }
					catch(IndexOutOfBoundsException e) {;}
					if(firstChar!='x' && !n.equals("")) {
						forceNames.add(n);
					}
					else {
						forceNames.add("");
						nbNullNames++;
					}
				}
				if(nbNullNames < forceNames.size()) { forceMode = true; }
				else { forceMode = false; }
			}
			//Set the structure
			else if(opt.startsWith("structure ")) {
				opt = opt.substring(10).trim(); //Delete "structure "
				char[] str = opt.toCharArray();
				for(char c : str) {
					if(c=='a' || c=='d' || c=='c' || c=='n' || c=='g' || c=='p' || c=='v') {
						structure.add(c);
					}
					else { averts.add("Type de mot non reconnu dans la structure: \""+c+"\""); }
				}
				if(structure.size() > 0) {structureDef = true; }
			}
			//Load XML dictionnary
			else if(opt.startsWith("load ")) {
				opt = opt.substring(5).trim();
				File file = new File(opt);
				if(file.exists()) { fileDico = opt; loadDico = true; }
				else { averts.add("Fichier non trouvé: \""+opt+"\""); }
			}
			else if(opt.trim().equals("help")) { help=true; }
			//Unrecognized option
			else if(!opt.trim().equals("")) { averts.add("Option non reconnue: \""+opt.trim()+"\""); }
		}
	}
}