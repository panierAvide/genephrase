/**
 * Window controller's package.
 * Controller associated to the window view.
 */
package genephrase.controller.window;