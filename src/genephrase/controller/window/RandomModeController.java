package genephrase.controller.window;

import genephrase.model.*;
import genephrase.view.window.RandomModePanel;
import java.awt.event.*;
import javax.swing.*;

/**
 * Reacts to events in RandomModePanel.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class RandomModeController implements ActionListener {
//ATTRIBUTES
	/** The current RandomModePanel. **/
	private RandomModePanel myView;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public RandomModeController(RandomModePanel view) {
		myView = view;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==myView.getGenerer()) {
			try {
				myView.getPhrase().setText("<html>"+myView.getSentenceGenerator().getRandomSentence()+"</html>");
			}
			catch(NoLemmasException err) {
				JOptionPane.showMessageDialog(myView, "Une liste de mots est vide, vous ne pouvez pas générer de phrase avec cette structure.\nPour remédier à ce problème, chargez un dictionnaire plus complet ou augmentez le nombre\nmaximum de lemmes.", "Liste de mots vide", JOptionPane.ERROR_MESSAGE);
			}
		}
		else if(e.getSource()==myView.getFavoris()) {
			//If the actual sentence isn't the default text, we add it to favorites
			if(!myView.getPhrase().getText().equals(myView.getDefaultText())) {
				String tmp = myView.getPhrase().getText();
				String newFav = tmp.substring(6, tmp.length()-8);
				if(!Options.favorites.contains(newFav)) {
					Options.favorites.add(newFav);
					Options.save();
					myView.getMainWindow().getFavoritesPanel().updateFavorites();
					JOptionPane.showMessageDialog(myView,"Ajouté aux favoris.");
				}
				else { JOptionPane.showMessageDialog(myView,"Déjà dans vos favoris !"); }
			}
		}
	}
}