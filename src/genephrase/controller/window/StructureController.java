package genephrase.controller.window;

import genephrase.view.window.StructureDialog;
import genephrase.view.window.ForceModePanel;
import java.awt.event.*;
import javax.swing.JOptionPane;

/**
 * Reacts to events in StructureDialog.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class StructureController implements ActionListener {
//ATTRIBUTES
	/** The current StructureDialog. **/
	private StructureDialog myView;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public StructureController(StructureDialog view) {
		myView = view;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==myView.getBdelete()) {
			//Delete the last element of the structure
			if(myView.getLocalStg().getStructure().size() > 0) {
				myView.getLocalStg().getStructure().remove(myView.getLocalStg().getStructure().size() - 1);
				myView.updateReadableStructure();
			}
		}
		else if(e.getSource()==myView.getBadjective()) {
			myView.getLocalStg().getStructure().add('a');
			myView.updateReadableStructure();
		}
		else if(e.getSource()==myView.getBadverb()) {
			myView.getLocalStg().getStructure().add('d');
			myView.updateReadableStructure();
		}
		else if(e.getSource()==myView.getBcomp()) {
			myView.getLocalStg().getStructure().add('c');
			myView.updateReadableStructure();
		}
		else if(e.getSource()==myView.getBcommonName()) {
			myView.getLocalStg().getStructure().add('n');
			myView.updateReadableStructure();
		}
		else if(e.getSource()==myView.getBproperName()) {
			myView.getLocalStg().getStructure().add('g');
			myView.updateReadableStructure();
		}
		else if(e.getSource()==myView.getBprep()) {
			myView.getLocalStg().getStructure().add('p');
			myView.updateReadableStructure();
		}
		else if(e.getSource()==myView.getBverb()) {
			myView.getLocalStg().getStructure().add('v');
			myView.updateReadableStructure();
		}
		else if(e.getSource()==myView.getBok()) {
			if(myView.getLocalStg().getStructure().size() > 0) {
				myView.getStg().setStructure(myView.getLocalStg().getStructure());
				myView.getMainWindow().getForceModePanel().updateTfs();
				myView.getMainWindow().getDictionnaryPanel().updateStatistics();
				myView.setVisible(false);
			}
			else {
				JOptionPane.showMessageDialog(myView, "Vous n'avez ajouté aucun type de mot à la structure !", "Structure vide", JOptionPane.ERROR_MESSAGE);
			}
		}
		else if(e.getSource()==myView.getBcancel()) {
			myView.setVisible(false);
		}
	}
}