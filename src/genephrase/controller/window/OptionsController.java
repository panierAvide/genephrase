package genephrase.controller.window;

import genephrase.model.Options;
import genephrase.view.window.OptionsDialog;
import java.awt.event.*;

/**
 * Reacts to events in OptionsDialog.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class OptionsController implements ActionListener {
//ATTRIBUTES
	/** The current OptionsDialog. **/
	private OptionsDialog myView;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public OptionsController(OptionsDialog view) {
		myView = view;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==myView.getLemmasAuto() || e.getSource()==myView.getLemmasMaxi()) {
			myView.getSpinner().setEnabled(false);
		}
		else if(e.getSource()==myView.getLemmasFixe()) {
			myView.getSpinner().setEnabled(true);
		}
		else if(e.getSource()==myView.getOk()) {
			//Param : max lemmas
			//Which option is selected ?
			if(myView.getLemmasAuto().isSelected()) {
				Options.maxLemmasLoad = 0;
			}
			else if(myView.getLemmasFixe().isSelected()) {
				Options.maxLemmasLoad = (Integer) myView.getSpinner().getValue();
			}
			else if(myView.getLemmasMaxi().isSelected()) {
				Options.maxLemmasLoad = -1;
			}
			//Save options and update dialog
			Options.save();
			myView.updateFields();
			myView.setVisible(false);
		}
		else if(e.getSource()==myView.getCancel()) {
			myView.setVisible(false);
			myView.updateFields();
		}
	}
}