package genephrase.controller.window;

import genephrase.model.Options;
import genephrase.view.window.FavoritesPanel;
import java.awt.event.*;
import javax.swing.event.*;

/**
 * Reacts to events in FavoritesPanel.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class FavoritesController implements ActionListener, ListSelectionListener {
//ATTRIBUTES
	/** The current FavoritesPanel. **/
	private FavoritesPanel myView;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public FavoritesController(FavoritesPanel view) {
		myView = view;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==myView.getBdelete()) {
			Options.favorites.remove(myView.getListe().getSelectedIndex());
			Options.save();
			myView.updateFavorites();
		}
	}

	/**
	 * Reacts to list selection changes.
	 */
	public void valueChanged(ListSelectionEvent e) {
		if(myView.getListe().getSelectedIndex() >= 0) {
			myView.getBdelete().setEnabled(true);
		}
		else { myView.getBdelete().setEnabled(false); }
	}
}