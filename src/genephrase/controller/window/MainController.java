package genephrase.controller.window;

import genephrase.model.*;
import genephrase.model.lang.SentenceGenerator;
import genephrase.view.window.MainWindow;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.*;

/**
 * Reacts to events in MainWindow.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class MainController extends WindowAdapter implements ActionListener {
//ATTRIBUTES
	/** The current MainWindow. **/
	private MainWindow myView;
	/** The current SentenceGenerator. **/
	private SentenceGenerator stg;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public MainController(MainWindow view, SentenceGenerator stg) {
		myView = view;
		this.stg = stg;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==myView.getLoadDictionnary()) {
			int retVal = JOptionPane.showOptionDialog(myView, "Charger un dictionnaire va supprimer le dictionnaire courant. Continuer ?", "Attention", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if(retVal==0) {
				JFileChooser fc = myView.getFileChooser();
				int returnVal = fc.showOpenDialog(myView);
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						myView.progress.setLocationRelativeTo(myView);
						myView.progress.setVisible(true);
						stg.setVocLists(XmlParser.readFile(file, Options.maxLemmasLoad));
						myView.getDictionnaryPanel().initTableContent();
						stg.saveDictionnaries(Options.FOLDER+"dico.bin");
					}
					catch(NoLemmasException err1) {
						JOptionPane.showMessageDialog(myView,"Aucun lemme trouvé dans ce dictionnaire.");
					}
					catch(FileNotFoundException err2) {
						System.out.println("Fichier non trouvé");
					}
					finally {
						myView.progress.setVisible(false);
					}
				}
			}
		}
		else if(e.getSource()==myView.getQuit()) {
			myView.close();
		}
		else if(e.getSource()==myView.getEditStructure()) {
			myView.getStructureDialog().setLocationRelativeTo(myView);
			myView.getStructureDialog().resetLocalStg();
			myView.getStructureDialog().updateReadableStructure();
			myView.getStructureDialog().setVisible(true);
		}
		else if(e.getSource()==myView.getPreferences()) {
			myView.getOptionsDialog().setLocationRelativeTo(myView);
			myView.getOptionsDialog().setVisible(true);
		}
		else if(e.getSource()==myView.getApropos()) {
			myView.getAboutDialog().setLocationRelativeTo(myView);
			myView.getAboutDialog().setVisible(true);
		}
	}

	/**
	 * Set the action performed when main window is closed.
	 */
	public void windowClosing(WindowEvent e) {
		myView.quit();
	}
}