package genephrase.controller.window;

import genephrase.view.window.AboutDialog;
import java.awt.event.*;

/**
 * Reacts to events in AboutDialog.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class AboutController implements ActionListener {
//ATTRIBUTES
	/** The current AboutDialog. **/
	private AboutDialog myView;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public AboutController(AboutDialog view) {
		myView = view;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==myView.getBok()) {
			myView.setVisible(false);
		}
	}
}