package genephrase.controller.window;

import genephrase.model.Options;
import genephrase.model.lang.*;
import genephrase.view.window.EditLemmaDialog;
import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.event.*;

/**
 * Reacts to events in EditLemmaDialog.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class EditLemmaController implements ActionListener {
//ATTRIBUTES
	/** The current EditLemmaDialog. **/
	private EditLemmaDialog myView;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public EditLemmaController(EditLemmaDialog view) {
		myView = view;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==myView.getBok()) {
			if(myView.getButtonGroup().getSelection() != null) {
				if(!myView.getTfLemma().getText().equals("")) {
					//Define the type as a character
					char type = 'x';
					if(myView.getRbAdjectif().isSelected()) { type = 'a'; }
					else if(myView.getRbAdverbe().isSelected()) { type = 'd'; }
					else if(myView.getRbComplement().isSelected()) { type = 'c'; }
					else if(myView.getRbNomCommun().isSelected()) { type = 'n'; }
					else if(myView.getRbNomPropre().isSelected()) { type = 'g'; }
					else if(myView.getRbPreposition().isSelected()) { type = 'p'; }
					else if(myView.getRbVerbe().isSelected()) { type = 'v'; }
					//Create the lemma
					Lemma l = new Lemma(myView.getTfLemma().getText(), type);
					//Add forms
					if(type=='v') {
						if(!myView.getTfSing1().getText().equals("")) { l.addForm(myView.getTfSing1().getText(), Lemma.SINGULAR, 1); }
						if(!myView.getTfSing2().getText().equals("")) { l.addForm(myView.getTfSing2().getText(), Lemma.SINGULAR, 2); }
						if(!myView.getTfSing3().getText().equals("")) { l.addForm(myView.getTfSing3().getText(), Lemma.SINGULAR, 3); }
						if(!myView.getTfPlur1().getText().equals("")) { l.addForm(myView.getTfPlur1().getText(), Lemma.PLURAL, 1); }
						if(!myView.getTfPlur2().getText().equals("")) { l.addForm(myView.getTfPlur2().getText(), Lemma.PLURAL, 2); }
						if(!myView.getTfPlur3().getText().equals("")) { l.addForm(myView.getTfPlur3().getText(), Lemma.PLURAL, 3); }
					}
					else if(type=='n' || type=='g' || type=='a') {
						if(!myView.getTfSingMasc().getText().equals("")) { l.addForm(myView.getTfSingMasc().getText(), Lemma.SINGULAR, Lemma.MASCULINE); }
						if(!myView.getTfSingFem().getText().equals("")) { l.addForm(myView.getTfSingFem().getText(), Lemma.SINGULAR, Lemma.FEMININE); }
						if(!myView.getTfPlurMasc().getText().equals("")) { l.addForm(myView.getTfPlurMasc().getText(), Lemma.PLURAL, Lemma.MASCULINE); }
						if(!myView.getTfPlurFem().getText().equals("")) { l.addForm(myView.getTfPlurFem().getText(), Lemma.PLURAL, Lemma.FEMININE); }
					}
					//In other cases, there's only the lemma
					//Add this lemma as new, or edit an old one ?
					if(myView.getCurrentLemma()==null) {
						//Add lemma to corresponding dictionnary
						myView.getDictionnaryPanel().getStg().getVocLists().get(type).getWords().add(l);
					}
					else {
						//Edit lemma
						myView.getDictionnaryPanel().getStg().getVocLists().get(type).getWords().set(
							myView.getDictionnaryPanel().getStg().getVocLists().get(type).getWords().indexOf(myView.getCurrentLemma()),
							l);
					}
					//Save dictionnaries
					myView.getDictionnaryPanel().getStg().saveDictionnaries(Options.FOLDER+"dico.bin");
					JOptionPane.showMessageDialog(myView, "Lemme édité avec succès.");
					myView.setVisible(false);
					myView.getDictionnaryPanel().initTableContent();
				}
				else { JOptionPane.showMessageDialog(myView, "Vous n'avez indiqué aucun lemme !"); }
			}
			else { JOptionPane.showMessageDialog(myView,"Vous n'avez choisi aucun type de mot !"); }
		}
		else if(e.getSource()==myView.getBcancel()) {
			myView.setVisible(false);
		}
		else if(e.getSource()==myView.getRbVerbe()) {
			myView.setMiddleContent(2);
		}
		else if(e.getSource()==myView.getRbNomPropre() || e.getSource()==myView.getRbNomCommun() || e.getSource()==myView.getRbAdjectif()) {
			myView.setMiddleContent(1);
		}
		else if(e.getSource()==myView.getRbComplement() || e.getSource()==myView.getRbPreposition() || e.getSource()==myView.getRbAdverbe()) {
			myView.setMiddleContent(3);
		}
	}
}