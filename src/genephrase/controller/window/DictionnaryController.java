package genephrase.controller.window;

import genephrase.model.Options;
import genephrase.view.window.DictionnaryPanel;
import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.event.*;

/**
 * Reacts to events in DictionnaryPanel.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class DictionnaryController extends MouseAdapter implements ActionListener, ListSelectionListener {
//ATTRIBUTES
	/** The current DictionnaryPanel. **/
	private DictionnaryPanel myView;
	/** The selected row in dictionnary table (-1 for none). **/
	private int selectedIndex = -1;

//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param view The associated view
	 */
	public DictionnaryController(DictionnaryPanel view) {
		myView = view;
	}

//OTHER METHODS
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == myView.getBadd()) {
			myView.getMainWindow().getEditLemmaDialog().setLemma(null, null);
			myView.getMainWindow().getEditLemmaDialog().setLocationRelativeTo(myView);
			myView.getMainWindow().getEditLemmaDialog().setVisible(true);
		}
		else if(e.getSource() == myView.getBedit()) {
			myView.getMainWindow().getEditLemmaDialog().setLemma(myView.getListIndexToLemma().get(selectedIndex), myView.getListIndexToType().get(selectedIndex));
			myView.getMainWindow().getEditLemmaDialog().setLocationRelativeTo(myView);
			myView.getMainWindow().getEditLemmaDialog().setVisible(true);
		}
		else if(e.getSource() == myView.getBdelete()) {
			int retVal = JOptionPane.showOptionDialog(myView, "Êtes-vous sûr de vouloir supprimer ce lemme ?", "Suppression", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if(retVal==0) {
				myView.getStg().getVocLists().get(myView.getListIndexToType().get(selectedIndex)).getWords().remove(myView.getListIndexToLemma().get(selectedIndex));
				myView.initTableContent();
				myView.getStg().saveDictionnaries(Options.FOLDER+"dico.bin");
			}
		}
	}

	/**
	 * Reacts to list selection changes.
	 */
	public void valueChanged(ListSelectionEvent e) {
		if(e.getSource() == myView.getTable().getSelectionModel() && myView.getTable().getRowSelectionAllowed()) {
			if(e.getFirstIndex() >= 0) {
				myView.getBedit().setEnabled(true);
				myView.getBdelete().setEnabled(true);
			}
			else {
				selectedIndex = -1;
				myView.getBedit().setEnabled(false);
				myView.getBdelete().setEnabled(false);
			}
		}
	}

	/**
	 * Reacts to mouse events on JTable.
	 */
	public void mouseClicked(MouseEvent e) {
		selectedIndex = myView.getTable().rowAtPoint(e.getPoint());
		if(selectedIndex >= 0) {
			myView.getBedit().setEnabled(true);
			myView.getBdelete().setEnabled(true);
		}
		else {
			myView.getBedit().setEnabled(false);
			myView.getBdelete().setEnabled(false);
		}
	}
}