package genephrase.test;

import genephrase.model.Options;

/**
 * Test class for Options.java
 * @author Adrien Pavie
 */
public class TestOptions {
	public static void main(String[] args) {
		Options.maxLemmasLoad = 12;
		System.out.println("MaxLemmasLoad : "+Options.maxLemmasLoad);
		//Test save and load
		Options.save();
		Options.maxLemmasLoad = 3;
		System.out.println("MaxLemmasLoad : "+Options.maxLemmasLoad);
		Options.load();
		System.out.println("MaxLemmasLoad : "+Options.maxLemmasLoad);
	}
}