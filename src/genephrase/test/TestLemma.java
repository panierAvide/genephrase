package genephrase.test;

import genephrase.model.lang.Lemma;

/**
 * Class test for Lemma.java.
 * @author Adrien Pavie
 */
public class TestLemma {
	public static void main(String[] args) {
		Lemma arroser = new Lemma("arroser",'v');
		arroser.addForm("arrosent",Lemma.PLURAL,3);
		arroser.addForm("arrossez",Lemma.PLURAL,2);
		arroser.addForm("arrosons",Lemma.PLURAL,1);
		arroser.addForm("arrose (3)",Lemma.SINGULAR,3);
		arroser.addForm("arroses",Lemma.SINGULAR,2);
		arroser.addForm("arrose (1)",Lemma.SINGULAR,1);
		//Test de getLemma()
		System.out.println("Lemme : "+arroser.getLemma());
		//Test de getForm()
		System.out.println("1e personne du pluriel : "+arroser.getForm(Lemma.PLURAL, 1));
		//Test de getRandomForm()
		int[] randomForm = arroser.getRandomForm();
		System.out.println("Nombre : "+randomForm[0]+", Personne : "+randomForm[1]+", Forme : "+arroser.getForm(randomForm[0],randomForm[1]));
		
		Lemma toto = new Lemma("toto",'g');
		System.out.println("Lemme : "+toto.getLemma());
		
		Lemma nageur = new Lemma("nageur",'n');
		nageur.addForm("nageuses", Lemma.PLURAL, Lemma.FEMININE);
		nageur.addForm("nageurs", Lemma.PLURAL, Lemma.MASCULINE);
		nageur.addForm("nageuse", Lemma.SINGULAR, Lemma.FEMININE);
		nageur.addForm("nageur", Lemma.SINGULAR, Lemma.MASCULINE);
		//getForm()
		System.out.println("Masculin pluriel : "+nageur.getForm(Lemma.PLURAL, Lemma.MASCULINE));
		//getRandomForm()
		randomForm = nageur.getRandomForm();
		System.out.println("Nombre : "+randomForm[0]+", Genre : "+randomForm[1]+", Forme : "+nageur.getForm(randomForm[0],randomForm[1]));
	}
}