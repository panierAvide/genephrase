package genephrase.test;

import genephrase.model.lang.*;
import genephrase.model.*;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Test class for SentenceGenerator.java.
 * Loads dictionnaries with XmlParser, creates a SentenceGenerator and generates a sentence.
 * @author Adrien Pavie
 */
public class TestSentenceGenerator {
	public static void main(String[] args) {
		if( (args[0]!=null) && (!args[0].equals("")) ) {
			//Create a default structure
			ArrayList<Character> str = new ArrayList<Character>();
			str.add('n');
			str.add('a');
			str.add('v');
			str.add('d');
			str.add('n');
			str.add('a');
			SentenceGenerator stg = new SentenceGenerator(str);
			//Initialization
			if(args[0].equals("init")) {
				try {
					stg.setVocLists(XmlParser.readFile(new File("data/dela-fr-public-u8.dic.xml"), 0));
					stg.saveDictionnaries("dico.bin");
				}
				catch(FileNotFoundException e) {
					System.out.println(e.getMessage());
				}
				catch(NoLemmasException e1) {;}
			}
			else if(args[0].equals("load")) {
				stg.loadDictionnaries("dico.bin");
			}
			try {
				System.out.println(stg.getRandomSentence());
			}
			catch(NoLemmasException e2) {;}
		}
		else {
			System.out.println("Argument manquant !");
		}
	}

	/*private static void time() {
		long currentTime = System.currentTimeMillis();
		System.out.println("["+(currentTime-lastTime)+"]");
		lastTime = currentTime;
	}*/
}