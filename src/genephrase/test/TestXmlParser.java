package genephrase.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.ArrayList;
import genephrase.model.XmlParser;
import genephrase.model.lang.*;

/**
 * Test class for XmlParser.java.
 * Tries to read a XML dictionnary.
 * @author Adrien Pavie
 */
public class TestXmlParser {
	public static void main(String[] args) {
		System.out.println("Analyse du dictionnaire...");
		try {
			HashMap<Character,Dictionnary> vocLists = XmlParser.readFile(new File("data/dela-fr-public-u8.dic.xml"),0);
			//Print nouns
			ArrayList<Lemma> nouns = vocLists.get('n').getWords();
			for(Lemma lemme : nouns) {
				System.out.println(lemme.getLemma());
			}
		}
		catch(FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("File not found.\n"+e1.getMessage());
		}
		catch(Exception e2) {
			e2.printStackTrace();
			System.out.println(e2.getMessage());
		}
	}
}