/**
 * Application tests' package.
 * Contains all test classes of the application.
 */
package genephrase.test;