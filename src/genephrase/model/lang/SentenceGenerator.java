package genephrase.model.lang;

import genephrase.model.NoLemmasException;
import java.io.*;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * SentenceGenerator allows you generate a sentence, following a specific syntax.
 * See {@link #setStructure(ArrayList)} for details about the structure.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class SentenceGenerator {
//ATTRIBUTES
	/** The list of dictionnaries, one dictionnary per type of word. **/
	private HashMap<Character,Dictionnary> vocLists;
	/** The structure to use to generate sentences. **/
	private ArrayList<Character> structure;

//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param struct The wanted sentence structure
	 */
	public SentenceGenerator(ArrayList<Character> struct) {
		structure = struct;
		//Create dictionnaries in the hashmap
		vocLists = new HashMap<Character,Dictionnary>(7);
		vocLists.put('n',new Dictionnary());
		vocLists.put('g',new Dictionnary());
		vocLists.put('v',new Dictionnary());
		vocLists.put('p',new Dictionnary());
		vocLists.put('a',new Dictionnary());
		vocLists.put('c',new Dictionnary());
		vocLists.put('d',new Dictionnary());
	}

	/**
	 * Class constructor.
	 * @param struct The wanted sentence structure
	 * @param dicos The list of dictionnaries
	 */
	public SentenceGenerator(ArrayList<Character> struct, HashMap<Character,Dictionnary> dicos) {
		structure = struct;
		vocLists = dicos;
	}

//ACCESSORS
	/**
	 * Get the set of dictionnaries.
	 * Keys in hashmap : a = adjective, c = complement, d = adverb, n = noun, p = preposition, v = verb.
	 * @return the hashmap of dictionnaries
	 */
	public HashMap<Character,Dictionnary> getVocLists() {
		return vocLists;
	}

	/**
	 * Get the structure of the sentence.
	 * @return An arraylist, with each type of word represented by a character.
	 */
	public ArrayList<Character> getStructure() {
		return structure;
	}

//MODIFIERS
	/**
	 * Set the sentence structure.
	 * Each word in the sentence is symbolized by a character. Its place in the sentence corresponds to its index in the arraylist.
	 * Characters : a = adjective, c = complement, d = adverb, n = noun, p = preposition, v = verb.
	 * @param struct The new sentence structure
	 */
	public void setStructure(ArrayList<Character> struct) {
		structure = struct;
	}

	/**
	 * Set the dictionnaries list.
	 * @param newList The new dictionnaries hashmap
	 */
	public void setVocLists(HashMap<Character,Dictionnary> newList) {
		vocLists = newList;
	}

//OTHER METHODS
	/**
	 * Generates a totally random sentence, following the current structure.
	 * @return The generated sentence
	 */
	public String getRandomSentence() throws NullPointerException, NoLemmasException {
		return getRandomSentence(null);
	}

	/**
	 * Generates a totally random sentence, following the current structure and forcing some parts.
	 * @param names The list of proper names to force
	 * @return The generated sentence
	 */
	public String getRandomSentence(ArrayList<String> names) throws NullPointerException, NoLemmasException {
		String result="";
		int prevNounNumber = -1;
		int prevNounGender = -1;
		int indexOfNames = 0;
		boolean verbAfterNoun = true;
		ArrayList<Lemma> waitingAdjectives = new ArrayList<Lemma>();
		for(int i=0; i<structure.size(); i++) {
			//Add a random word for this type in sentence, upper case the first letter in case of a noun
			char type = structure.get(i);
			String currentWord="";
			int allowedNull = 0;
			while( (currentWord.equals("") || currentWord.equals("<null>")) && allowedNull < 5) {
				try {
					Lemma currentLemma = vocLists.get(type).getRandomWord();
					if(type=='n' || type=='g') {
						int[] randomForm = null;
						if(type=='g' && names != null && !names.get(indexOfNames).equals("")) {
							randomForm = new int[2];
							randomForm[0] = Lemma.SINGULAR;
							randomForm[1] = Lemma.MASCULINE;
							currentWord = names.get(indexOfNames);
						}
						else {
							randomForm = currentLemma.getRandomForm();
							currentWord = currentLemma.getForm(randomForm[0], randomForm[1]);
						}
						if(type=='g') { indexOfNames++; }
						prevNounNumber = randomForm[0];
						prevNounGender = randomForm[1];
						verbAfterNoun=false;
						//Edit previous adjectives if there's any
						if(waitingAdjectives.size()>0) {
							String adj = null;
							//if(type=='n') {
								adj = waitingAdjectives.get(0).getForm(randomForm[0], randomForm[1]);
							/*}
							else {
								adj = waitingAdjectives.get(0).getForm(Lemma.SINGULAR, Lemma.MASCULINE);
							}*/
							//We replace the reference by the adjective.
							if(type=='n') { result = result.replaceAll("adj0adj", Lemma.getDeterminer(adj, randomForm[1], randomForm[0])+adj); }
							else { result = result.replaceAll("adj0adj", adj); }
							for(int j=1; j<waitingAdjectives.size(); j++) {
								if(type=='n') {
									adj = waitingAdjectives.get(j).getForm(randomForm[0], randomForm[1]);
								}
								else {
									adj = waitingAdjectives.get(j).getForm(Lemma.SINGULAR, Lemma.MASCULINE);
								}
								result = result.replaceAll("adj"+j+"adj", adj);
							}
							waitingAdjectives.clear();
						}
						else if(type=='n') {
							currentWord = Lemma.addDeterminer(currentWord, randomForm[1], randomForm[0]);
						}
					}
					else if(type=='v') {
						if(prevNounNumber==-1) { currentWord = currentLemma.getLemma(); }
						else { currentWord = currentLemma.getForm(prevNounNumber, 3); }
						verbAfterNoun=true;
					}
					else if(type=='a') {
						//If there's no verb between last noun and current adjective, gender and number are the same as last noun.
						if(!verbAfterNoun) {
							currentWord = currentLemma.getForm(prevNounNumber, prevNounGender);
						}
						//If noun is after adjective, we place a code to edit it later
						else {
							int index = waitingAdjectives.size();
							waitingAdjectives.add(currentLemma);
							currentWord = "adj"+index+"adj";
						}
					}
					else {
						currentWord = currentLemma.getLemma();
					}
					result += currentWord+" ";
				}
				catch(NullPointerException e) {
					currentWord = "<null>";
					allowedNull++;
				}
			}
			if(currentWord.equals("<null>")) { throw new NoLemmasException("Une des listes de mots est vide, la phrase ne peut être générée."); }
		}
		result = result.replaceAll("adj\\d+adj", "");
		result = correctSentence(result.substring(0,1).toUpperCase()+result.substring(1).trim()+".");
		return result;
	}

	/**
	 * Deletes errors which are words-independent (visible only when the sentence is finished).
	 * @param sentence The sentence to correct
	 * @return The corrected sentence
	 */
	public String correctSentence(String sentence) {
		sentence = sentence.replaceAll("à les", "aux");
		sentence = sentence.replaceAll("à le", "au");
		sentence = sentence.replaceAll("de le", "du");
		sentence = sentence.replaceAll("  ", " ");
		return sentence;
	}

	/**
	 * Loads dictionnaries from the file system.
	 * @param filename The file name on the disk
	 */
	public void loadDictionnaries(String filename) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);
			vocLists = (HashMap<Character,Dictionnary>) ois.readObject();
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
		catch(ClassNotFoundException e1) {
			System.out.println(e1.getMessage());
		}
	}

	/**
	 * Saves dictionnaries on the file system.
	 * @param filename The file name
	 */
	public void saveDictionnaries(String filename) {
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(vocLists);
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
	}
}