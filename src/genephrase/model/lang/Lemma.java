package genephrase.model.lang;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * This class represents the lemma and all forms which exists of a word.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class Lemma implements Serializable {
//ATTRIBUTES
	/** Current version of the class (for serialization). **/
	private static final long serialVersionUID = 1L;
	//Grammatical attributes
	/** To represent a masculine word. **/
	public static final int MASCULINE = 0;
	/** To represent a feminine word. **/
	public static final int FEMININE = 1;
	/** To represent a singular word. **/
	public static final int SINGULAR = 0;
	/** To represent a plural word. **/
	public static final int PLURAL = 1;
	//Lemma and forms
	/** The lemma as a string. **/
	private String lemma;
	/** All forms of this lemma. First dimension is for number (singular or plural), the second dimension is for person (1,2,3 if it's a verb) or gender (masculine or feminine). **/
	private String[][] forms;
	/** Type of word (a = adjective, c = complement, d = adverb, n = noun, g = proper noun, p = preposition, v = verb). **/
	private char type;

//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param lemma The current word's lemma.
	 * @param type The type of word (a = adjective, c = complement, d = adverb, n = noun, g = proper noun, p = preposition, v = verb).
	 */
	public Lemma(String lemma, char type) {
		this.lemma = lemma;
		this.type = type;
		if(type=='v') {
			forms = new String[2][3];
		}
		else {
			forms = new String[2][2];
		}
	}

//ACCESSORS
	/**
	 * Returns the lemma.
	 * @return The lemma
	 */
	public String getLemma() {
		return lemma;
	}
	
	/**
	 * Returns the wanted form of this word.
	 * @param number The grammatical number of the word (use {@link #SINGULAR} or {@link #PLURAL}).
	 * @param param2 If it's a noun, the gender of the word (use {@link #MASCULINE} or {@link #FEMININE}). If it's a verb, the person of the word (it has to be 1, 2 or 3).
	 * @return The wanted form
	 */
	public String getForm(int number, int param2) throws NullPointerException {
		String result;
		
		//If this lemma is a verb, param2 is the person, and is from 1 to 3. Array index is from 0 to 2
		if(type=='v') { param2--; }
		
		//If the wanted form exists, it's simple
		if( number >= 0 && param2 >=0 && number <=1 && ( param2 <= 1 || (type=='v' && param2<=2) ) && forms[number][param2] != null) {
			result = forms[number][param2];
		}
		//If it doesn't exists, but it's and adverb, adjective or preposition, we return the lemma
		else if( type=='p' || type=='d' || type=='a' || type=='g' ) {
			result = lemma;
		}
		else {
			throw new NullPointerException("Form not found !");
		}
		
		return result;
	}

	/**
	 * Get a random form.
	 * @return An array, which contains : at index 0 the number, at index 1 the gender (noun) or person (verb).
	 */
	public int[] getRandomForm() {
		Random rand = new Random();
		int randNumber = -1;
		int randParam2 = -1;
		//Random number
		ArrayList<Integer> possibleNumber = new ArrayList<Integer>(2);
		if(forms[SINGULAR] != null) { possibleNumber.add(SINGULAR); }
		if(forms[PLURAL] != null) { possibleNumber.add(PLURAL); }
		if(possibleNumber.size() > 0) {
			randNumber = possibleNumber.get(rand.nextInt(possibleNumber.size()));
			
			ArrayList<Integer> possibleParam2 = new ArrayList<Integer>(3);
			if(forms[randNumber][0]!=null) { possibleParam2.add(0); }
			if(forms[randNumber][1]!=null) { possibleParam2.add(1); }
			if(type=='v' && forms[randNumber][2]!=null) { possibleParam2.add(2); }
			if(possibleParam2.size() > 0) {
				randParam2 = possibleParam2.get(rand.nextInt(possibleParam2.size()));
			}
		}

		//Then create the return array
		if(type=='v' && randParam2 != -1) { randParam2++; }
		int[] result = { randNumber, randParam2 };
		//System.out.println("GetRandomForm : "+Arrays.toString(result));
		return result;
	}

//MODIFIERS
	/**
	 * Adds a form.
	 * @param form The string representation
	 * @param number The grammatical number of the word (use {@link #SINGULAR} or {@link #PLURAL}).
	 * @param param2 If it's a noun, the gender of the word (use {@link #MASCULINE} or {@link #FEMININE}). If it's a verb, the person of the word (it has to be 1, 2 or 3).
	 */
	public void addForm(String form, int number, int param2) {
		if(type=='v') {
			param2--;
		}
		if(number >= 0 && param2 >= 0) {
			forms[number][param2] = form;
		}
	}

//OTHER METHODS
	/**
	 * Adds a determiner to a noun.
	 * @param noun The noun
	 * @param gender The noun's gender
	 * @param number The noun's number
	 * @return The noun with its determiner
	 */
	public static String addDeterminer(String noun, int gender, int number) {
		String result;
		char firstLetter = noun.toLowerCase().charAt(0);
		//If it starts with a vowel, we add a " l' ".
		if(number==SINGULAR && (firstLetter=='a' || firstLetter=='e' || firstLetter=='y' || firstLetter=='u' || firstLetter=='i' || firstLetter=='o' || firstLetter=='a' || firstLetter=='é' || firstLetter=='è' || firstLetter=='à' || firstLetter=='ù' || firstLetter=='ä' || firstLetter=='ë' || firstLetter=='ö' || firstLetter=='ï' || firstLetter=='ü' || firstLetter=='ÿ' || firstLetter=='â' || firstLetter=='ê' || firstLetter=='î' || firstLetter=='ô' || firstLetter=='û' || firstLetter=='ŷ' )) {
			result = "l'"+noun;
		}
		//If it starts with a h, we add "un","une","des"
		else if(firstLetter=='h') {
			if(noun.toLowerCase().equals("hakka") || noun.toLowerCase().equals("hégire")) {
				result = "l'"+noun;
			}
			else if(noun.toLowerCase().equals("hannouka")) {
				result = "la "+noun;
			}
			else {
				if(number==PLURAL) {
					result = "des "+noun;
				}
				else if(gender==MASCULINE) {
					result = "un "+noun;
				}
				else {
					result = "une "+noun;
				}
			}
		}
		//Else, we add "le", "la", "les"
		else {
			if(number==PLURAL) {
				result = "les "+noun;
			}
			else if(gender==MASCULINE) {
				result = "le "+noun;
			}
			else {
				result = "la "+noun;
			}
		}
		return result;
	}

	/**
	 * Get the determiner corresponding to a noun which have an adjective before it.
	 * @param adjective The adjective
	 * @param gender The noun's gender
	 * @param number The noun's number
	 * @return The determiner
	 */
	public static String getDeterminer(String adjective, int gender, int number) {
		String result;
		char firstLetter = adjective.toLowerCase().charAt(0);
		//If it starts with a vowel or a h, we add a " l' ".
		if(firstLetter=='h' || (number==SINGULAR && (firstLetter=='a' || firstLetter=='e' || firstLetter=='y' || firstLetter=='u' || firstLetter=='i' || firstLetter=='o' || firstLetter=='a' || firstLetter=='é' || firstLetter=='è' || firstLetter=='à' || firstLetter=='ù' || firstLetter=='ä' || firstLetter=='ë' || firstLetter=='ö' || firstLetter=='ï' || firstLetter=='ü' || firstLetter=='ÿ' || firstLetter=='â' || firstLetter=='ê' || firstLetter=='î' || firstLetter=='ô' || firstLetter=='û' || firstLetter=='ŷ' ))) {
			result = "l'";
		}
		//Else, we add "le", "la", "les"
		else {
			if(number==PLURAL) {
				result = "les ";
			}
			else if(gender==MASCULINE) {
				result = "le ";
			}
			else {
				result = "la ";
			}
		}
		return result;
	}
}