/**
 * Language related classes.
 * Contains all the classes that represents a language aspect.
 */
package genephrase.model.lang;