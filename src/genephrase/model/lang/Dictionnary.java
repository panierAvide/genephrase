package genephrase.model.lang;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class modelize a list of words, like a dictionnary.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class Dictionnary implements Serializable {
//ATTRIBUTES
	/** The list of lemmas contained in the dictionnary. **/
	private ArrayList<Lemma> words;
	/** Version of the class (for serialization). **/
	private static final long serialVersionUID = 2L;

//CONSTRUCTOR
	/**
	 * Class constructor.
	 */
	public Dictionnary() {
		words = new ArrayList<Lemma>();
	}

//ACCESSORS
	/**
	 * Get the words list.
	 * @return The words arraylist
	 */
	public ArrayList<Lemma> getWords() {
		return words;
	}

	/**
	 * Get a random word choosen from the list.
	 * @return the random word
	 */
	public Lemma getRandomWord() throws NullPointerException {
		if(words.size() > 0) {
			int max = words.size() - 1;
			int rand = (int) (Math.random()*max);
			return words.get(rand);
		}
		else {
			throw new NullPointerException("La liste de mots est vide");
		}
	}
}