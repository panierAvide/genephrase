package genephrase.model;

/**
 * Thrown when a file doesn't contain any lemmas.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class NoLemmasException extends Exception {
//ATTRIBUTES
	/** Version of class (for serialization). **/
	private static final long serialVersionUID = 1L;

//CONSTRUCTORS
	/**
	 * Class constructor.
	 */
	public NoLemmasException() {
		super();
	}

	/**
	 * Class constructor.
	 */
	public NoLemmasException(String leMessage) {
		super(leMessage);
	}
}