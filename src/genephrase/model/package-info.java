/**
 * Application model's package.
 * Model is the representation of the real world in MVC model.
 */
package genephrase.model;