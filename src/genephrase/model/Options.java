package genephrase.model;

import java.io.*;
import java.util.ArrayList;

/**
 * Represents options in this software.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 */
public class Options {
//ATTRIBUTES
	/**
	 * Defines the amount of lemmas to load when a XML dictionnary is analyzed.
	 * Values : -1 to load the complete dictionnary, 0 to let the program decide, > 0 to use an user determined number.
	 * Default value : 0.
	 */
	public static int maxLemmasLoad = 0;
	/** The favorites' list. **/
	public static ArrayList<String> favorites = new ArrayList<String>();
	/** The folder where application saves its parameters. **/
	public static final String FOLDER=System.getProperty("user.home")+"/.genephrase/";

//OTHER METHODS
	/**
	 * Save options on the filesystem.
	 */
	public static void save() {
		try {
			File file = new File(FOLDER);
			if(file.exists() || file.mkdirs()) {
				FileOutputStream fos = new FileOutputStream(FOLDER+"options.bin");
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				//Parameters to save
				oos.writeInt(maxLemmasLoad);
				oos.writeObject(favorites);
				//Close streams
				oos.close();
				fos.close();
			}
			else { throw new RuntimeException("Options can't be saved."); }
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Load options from the filesystem.
	 */
	public static void load() {
		try {
			FileInputStream fis = new FileInputStream(FOLDER+"options.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			//Parameters to load
			maxLemmasLoad = ois.readInt();
			favorites = (ArrayList<String>) ois.readObject();
			//Close streams
			ois.close();
			fis.close();
			//Default parameters if null
			if(favorites==null) { favorites = new ArrayList<String>(); }
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
		catch(ClassNotFoundException e1) {
			System.out.println(e1.getMessage());
		}
	}
}