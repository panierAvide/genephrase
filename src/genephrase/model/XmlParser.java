package genephrase.model;

import genephrase.model.lang.*;
import java.io.*;
import java.util.HashMap;

/**
 * XML Parser for Genephrase.
 * Used to analyze XML dictionnaries.
 * @author Adrien Pavie
 * @version 1.0
 * @history
	1.0	Creation of the class
 * @todo
	Resolve the problem with accents under windows
	Add a loading bar ?
 */
public class XmlParser {
//OTHER METHODS
	/**
	 * Reads a XML dictionnary.
	 * @param filename The file
	 * @param maxSize The maximal quantity of lemmas which can be loaded (-1 for all, 0 for auto)
	 * @return A hashmap which contains different dictionnaries (Keys : a = adjective, c = complement, d = adverb, n = noun, p = preposition, v = verb).
	 */
	public static HashMap<Character,Dictionnary> readFile(File filename, int maxSize) throws FileNotFoundException, NoLemmasException {
		System.out.println("Chargement d'un dictionnaire:");
		//Reader variables
		boolean endOfFile = false;
		String currentLine;
		BufferedReader reader;
		//Analyze variables
		Lemma currentLemma = null;
		String currentLemmaStr = null;
		char currentType = 'x';
		String currentForm = null;
		int currentNumber = -1;
		int currentParam2 = -1;
		String currentTense = null;
		String currentSubcat = "";
		boolean currentColl = false;
		boolean currentCompound = false;
		int nbForms=0;
		int nbLemmas=0;
		int maxLemmas = countWord(filename, "<lemma>");
		System.out.println(" Dictionnaire XML : "+maxLemmas+" lemmes");
		HashMap<String,Character> types = new HashMap<String,Character>();
		types.put("noun",'n');
		types.put("GN",'n');
		//types.put("GNP",'n');
		//types.put("GNPX",'n');
		types.put("verb",'v');
		types.put("prep",'p');
		types.put("adj",'a');
		types.put("adverb",'d');
		//Calculate how many lemmas can be loaded
		if(maxSize == 0) {
			int lemmasGb = 150000; //The reference : how many lemmas for 1 GB (1 073 741 824 bytes)
			long heapMaxSize = Runtime.getRuntime().maxMemory(); //The allowed memory for Java
			long maxSizeLg = lemmasGb * heapMaxSize / 1073741824;
			maxSize = (int) maxSizeLg;
			System.out.println(" Maximum en mémoire : "+maxSize+" lemmes");
		}
		//MaxSize : for x lemmas, how many do we save ?
		int maxForXLemmas=0;
		int xLemmas = 100;
		int countLemmas=0; // Counter for lemma limitation
		if(maxSize > 0) {
			double maxTmp = (double) maxSize / (double) maxLemmas * (double) xLemmas;
			maxForXLemmas = (int) Math.rint(maxTmp);
			System.out.println(" Ratio de sauvegarde : "+maxForXLemmas+" / "+xLemmas+" lemmes");
			if(maxForXLemmas >= xLemmas) { maxSize = -1; } //If we save more than 10 lemmas for 10 lemmas, we can save all.
		}
		//Result variable
		HashMap<Character,Dictionnary> result = new HashMap<Character,Dictionnary>(6);
		result.put('n',new Dictionnary());
		result.put('v',new Dictionnary());
		result.put('p',new Dictionnary());
		result.put('a',new Dictionnary());
		result.put('c',new Dictionnary());
		result.put('d',new Dictionnary());
		result.put('g',new Dictionnary());
		try {
			System.out.println(" Analyse du dictionnaire...");
			reader = new BufferedReader(new FileReader(filename));
			while(!endOfFile && (maxSize==-1 || nbLemmas < maxSize)) {
				currentLine = reader.readLine();
				//End of file ?
				if(currentLine==null) {
					endOfFile = true;
				}
				//Line analysis
				else {
					currentLine = currentLine.trim();
					//Get the lemma
					if(currentLine.startsWith("<lemma>")) {
						currentLemmaStr = currentLine.substring(7, currentLine.indexOf("</lemma>")).replaceAll("\\\\", "");
					}
					//Get the type
					else if(currentLine.startsWith("<pos")) {
						String currentTypeStr = currentLine.substring(currentLine.indexOf("name='")+6, currentLine.indexOf("'/>"));
						if(types.containsKey(currentTypeStr)) {
							currentType = types.get(currentTypeStr);
							//Create the lemma object if lemma isn't null
							if(currentLemmaStr!=null) {
								currentLemma = new Lemma(currentLemmaStr, currentType);
							}
							//else { System.out.println("Warning : try to create a lemma object without a lemma !"); }
						}
						//else { System.out.println("Warning : unknown type !"); }
					}
					//Get the form
					else if(currentLine.startsWith("<form>")) {
						currentForm = currentLine.substring(6, currentLine.indexOf("</form>")).replaceAll("\\\\", "");
					}
					//Get the parameters
					else if(currentLine.startsWith("<feat")) {
						String paramName = currentLine.substring(currentLine.indexOf("name='")+6, currentLine.indexOf("' "));
						//Number parameter
						if(paramName.equals("number")) {
							String currentNumberStr = currentLine.substring(currentLine.indexOf("value='")+7, currentLine.indexOf("'/>")).toLowerCase();
							if(currentNumberStr.equals("singular")) {
								currentNumber = Lemma.SINGULAR;
							}
							else if(currentNumberStr.equals("plural")) {
								currentNumber = Lemma.PLURAL;
							}
						}
						//Tense parameter
						else if(paramName.equals("tense")) {
							currentTense = currentLine.substring(currentLine.indexOf("value='")+7, currentLine.indexOf("'/>")).toLowerCase();
						}
						//Gender parameter
						else if(paramName.equals("gender")) {
							String currentParam2Str = currentLine.substring(currentLine.indexOf("value='")+7, currentLine.indexOf("'/>")).toLowerCase();
							if(currentParam2Str.equals("masculine")) {
								currentParam2 = Lemma.MASCULINE;
							}
							else if(currentParam2Str.equals("feminine")) {
								currentParam2 = Lemma.FEMININE;
							}
						}
						//Person parameter
						else if(paramName.equals("person")) {
							String currentParam2Str = currentLine.substring(currentLine.indexOf("value='")+7, currentLine.indexOf("'/>"));
							int tmpPerson = Integer.parseInt(currentParam2Str);
							if(tmpPerson >= 1 && tmpPerson <= 3) {
								currentParam2 = tmpPerson;
							}
						}
						//Subcategories
						else if(paramName.equals("subcat")) {
							currentSubcat = currentLine.substring(currentLine.indexOf("value='")+7, currentLine.indexOf("'/>"));
						}
						//Collective ?
						else if(paramName.equals("coll")) {
							if(currentLine.substring(currentLine.indexOf("value='")+7, currentLine.indexOf("'/>")).equals("true")) {
								currentColl = true;
							}
						}
						//Compound
						else if(paramName.equals("compound")) {
							if(currentLine.substring(currentLine.indexOf("value='")+7, currentLine.indexOf("'/>")).equals("comp")) {
								currentCompound = true;
							}
						}
					}
					//Save the form
					else if(currentLine.startsWith("</inflected>")) {
						//Is current lemma null ?
						if(currentLemma!=null) {
							//Are all parameters okay ?
							if(currentForm!=null && currentNumber >= 0 && currentParam2 >= 0) {
								currentLemma.addForm(currentForm, currentNumber, currentParam2);
								//Count this form
								nbForms++;
							}
							//TODO : ne rentre pas dans la condition suivante
							//Some adverbs and adjectives have one form which always works
							else if(currentForm!=null && (currentType=='a' || currentType=='d' || currentType=='p')) {
								currentLemma.addForm(currentForm, Lemma.SINGULAR, Lemma.MASCULINE);
								currentLemma.addForm(currentForm, Lemma.SINGULAR, Lemma.FEMININE);
								currentLemma.addForm(currentForm, Lemma.PLURAL, Lemma.MASCULINE);
								currentLemma.addForm(currentForm, Lemma.PLURAL, Lemma.FEMININE);
								nbForms += 4;
							}
							//else { System.out.println("Warning : form not added, incorrect parameters !"); }
						}
						//else { System.out.println("Warning : form not added, lemma doesn't exist !"); }
						//Reset vars
						currentForm=null;
						currentNumber=-1;
						currentParam2=-1;
						currentTense=null;
					}
					//Save the lemma, add it to the dictionnary
					else if(currentLine.startsWith("</entry>")) {
						//Is current lemma null ?
						if(currentLemma!=null) {
							//Verify whether lemma contains form, or doesn't need to
							if(nbForms > 0 || currentType=='p' || currentType=='c' || currentType=='d') {
								if(maxSize==-1 || countLemmas<=maxForXLemmas) {
									//If first character of the lemma is upper case, it's a proper name
									if(currentSubcat.equals("human") && currentColl==false && isProperNoun(currentLemmaStr)) {
										result.get('g').getWords().add(currentLemma);
									}
									else {
										result.get(currentType).getWords().add(currentLemma);
									}
									nbLemmas++;
								}
								countLemmas++;
								if(countLemmas==xLemmas) { countLemmas = 0; }
							}
							//else { System.out.println("Warning : lemma not added, needs forms and doesn't contain one !"); }
						}
						//else { System.out.println("Warning : lemma not added, it doesn't exist !"); }
						//Reinitialize vars
						currentLemma=null;
						currentLemmaStr=null;
						currentForm=null;
						currentNumber=-1;
						currentParam2=-1;
						currentTense=null;
						currentType='x';
						currentSubcat="";
						currentColl = false;
						currentCompound = false;
						nbForms=0;
					}
				}
			}
			System.out.println(" Chargés en mémoire : "+nbLemmas+" lemmes");
			if(nbLemmas==0) { throw new NoLemmasException(); }
		}
		catch(IOException e2) {
			e2.printStackTrace();
			System.out.println("Erreur I/O.\n"+e2.getMessage());
		}
		return result;
	}

	/**
	 * Counts in how many lines a word appears in a file.
	 * @param filename The file
	 * @param word The wanted word
	 * @return The count
	 */
	private static int countWord(File filename, String word) {
		int count = 0;
		try {
			FileReader fr = new FileReader(filename);
			BufferedReader reader = new BufferedReader(fr);
			boolean endOfFile = false;
			String line;
			while(!endOfFile) {
				line = reader.readLine();
				if(line==null) {
					endOfFile = true;
				}
				else {
					if(line.indexOf(word) >= 0) {
						count++;
					}
				}
			}
			fr.close();
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
		return count;
	}

	/**
	 * Is a string a proper noun ?
	 * @param str The string to analyze
	 * @return True if it's a proper noun, false if not.
	 */
	private static boolean isProperNoun(String str) {
		boolean result = false;
		//First letter is upper case
		if(Character.isUpperCase(str.charAt(0))) {
			//Doesn't have a "-" without " "
			if(str.indexOf("-") < 0 || str.indexOf(" ") > 0) {
				//Has a family name with a first letter which is upper case, or doesn't have one
				if(str.indexOf(" ") < 0 || Character.isUpperCase(str.charAt(str.indexOf(" ")+1)) || Character.isUpperCase(str.charAt(str.indexOf(" de ")+4))) {
					result = true;
				}
			}
		}
		return result;
	}
}