# Généphrase

## Table of Contents

* Table of Contents
* Introduction
* Installation/Launch instructions
* Build Instructions
* Development
* License Agreement



## Introduction

This is the first version of Généphrase. It's fully working and complete, but if you find
something that can be improved, a bug or just want to comment, please create a new issue or contact one of the
contributors.

Généphrase can run under all operating systems where Java runs. It includes major OS like Microsoft Windows,
GNU/Linux distributions and Mac OS. Généphrase needs only Java Runtime Environment 7 or newer. Download it
at http://www.java.com/.



## Installation/Launch instructions

Généphrase doesn't need any installation. Just double-click on genephrase-1.0.jar or launch the following
command in a terminal:  
`java -jar genephrase-1.0.jar`

To show help (and the list of all options) in terminal:  
`java -jar genephrase-1.0.jar console --help`


You must load a XML dictionnary to have a good basis for generate sentences. The archive contains
a file named `dela-fr-public-u8.dic.xml`. It's a XML dictionnary for French language. It's fully compatible
with Genephrase.  
To load it in window mode, just do "Fichier > Charger un dictionnaire" and select this file.  
To load it in console mode, add the option `--load <file>` to the command (where file points to the wanted
dictionnary).



## Build Instructions

__You need to do the following instructions only if you want to compile Généphrase from source code.__

Généphrase use Ant to run automatically compilation, tests and application launch. You need to install it
before trying to compile source code. You can find it at http://ant.apache.org/.

The build.xml file is located in ant/ folder. To see all available commands, just run in a terminal:  
`cd ant/`

Then  
`ant -p`

Before trying to compile, you need to init some parameters. Just run:  
`ant init`

Then, to compile, run:  
`cd ../ww/`  
`ant compile`

Finally, to launch Généphrase, run:  
`ant run`

Or to launch in console mode:  
`ant runConsole`

That's all.



## Development

If you are interested in Généphrase and want to join development team, just start commit the project.

There's 2 branches:
* Master: the main branch, which contains stable version of Genephrase. This one contains only fully tested features and bug corrections.
* Feature: this branch contains new features, it's a development branch and could be unstable.

Choose the branch which corresponds to your current task.


## License Agreement

You MUST agree the GNU General Public License version 3 (see LICENSE.txt) before using Généphrase.

NOTE: This program includes extra software or script:

* data/dela-fr-public-u8.dic.xml
** French XML dictionnary (by LIGM, Équipe d'informatique linguistique)
** Download: http://infolingu.univ-mlv.fr/DonneesLinguistiques/Dictionnaires/telechargement.html
** License:  LGPLLR (http://infolingu.univ-mlv.fr/DonneesLinguistiques/Lexiques-Grammaires/lgpllr.html)
