==========================================
|                                        |
|               Généphrase               |
|         Version 1.0 (Août 2012)        |
|     Copyright (c) Adrien Pavie 2012    |
|                                        |
==========================================

= Table of Contents =

* Table of Contents
* Introduction
* Installation/Launch instructions
* Build Instructions
* Development
* License Agreement



= Introduction =

Thank you for downloading Généphrase from http://www.line2soft.net or
from https://bitbucket.org/panierAvide/genephrase/overview. If you downloaded this zip-file
from another site, please check https://bitbucket.org/panierAvide/genephrase/overview for
the latest official release.

This is the first version of Généphrase. It's fully working and complete, but if you find
something that can be improved, a bug or just want to comment, please contact us in the aforementioned
links.

Généphrase can run under all operating systems where Java runs. It includes major OS like Microsoft Windows,
GNU/Linux distributions and Mac OS. Généphrase needs only Java Runtime Environment 7 or newer. Download it
at http://www.java.com/.

Developed by
- Adrien Pavie <panierAvide at laposte dot net>



= Installation/Launch instructions =

Généphrase doesn't need any installation. Just double-click on genephrase-1.0.jar or launch the following
command in a terminal:
`java -jar genephrase-1.0.jar`

To show help (and the list of all options) in terminal:
`java -jar genephrase-1.0.jar console --help`


You must load a XML dictionnary to have a good basis for generate sentences. The archive contains
a file named `dela-fr-public-u8.dic.xml`. It's a XML dictionnary for French language. It's fully compatible
with Genephrase.
To load it in window mode, just do "Fichier > Charger un dictionnaire" and select this file.
To load it in console mode, add the option `--load <file>` to the command (where file points to the wanted
dictionnary).


= Build Instructions =

__You need to do the following instructions only if you want to compile Généphrase from source code.__

Généphrase use Ant to run automatically compilation, tests and application launch. You need to install it
before trying to compile source code. You can find it at http://ant.apache.org/.

The build.xml file is located in ant/ folder. To see all available commands, just run in a terminal:
`cd ant/`
Then
`ant -p`

Before trying to compile, you need to init some parameters. Just run:
`ant init`

Then, to compile, run:
`cd ../ww/`
`ant compile`

Finally, to launch Généphrase, run:
`ant run`

Or to launch in console mode:
`ant runConsole`

That's all.



= Development =

If you are interested in Généphrase and want to join development team, go
to https://bitbucket.org/panierAvide/genephrase/overview. You will find more informations about
development.



= License Agreement =

You MUST agree the GNU General Public License version 3 (see LICENSE.txt) before using Généphrase.

NOTE: This program includes extra software or script:
   * data/dela-fr-public-u8.dic.xml - French XML dictionnary (by LIGM, Équipe d'informatique linguistique)
                                      Download: http://infolingu.univ-mlv.fr/DonneesLinguistiques/Dictionnaires/telechargement.html
                                      License:  LGPLLR (http://infolingu.univ-mlv.fr/DonneesLinguistiques/Lexiques-Grammaires/lgpllr.html)
