==========================================
|                                        |
|               Généphrase               |
|         Version 1.0 (Août 2012)        |
|     Copyright (c) Adrien Pavie 2012    |
|                                        |
==========================================

= Sommaire =

* Sommaire
* Introduction
* Instructions pour l'installation et le lancement
* Instructions de compilation
* Développement
* Acceptation de la licence



= Introduction =

Merci d'avoir téléchargé Généphrase depuis http://www.line2soft.net ou
depuis https://bitbucket.org/panierAvide/genephrase/overview. Si vous avez téléchargé cette archive
depuis un autre site web, consultez https://bitbucket.org/panierAvide/genephrase/overview pour
obtenir la version officielle la plus récente.

Ceci est la première version de Généphrase. Elle est entièrement fonctionelle et complète, mais dans
le cas où vous trouveriez un aspect pouvant être amélioré, un bogue ou que vous souhaitez juste donner
votre avis, contactez-nous via les adresses citées précédemment.

Généphrase peut fonctionner sous tout système d'exploitation supportant Java. Cela comprend les OS majeurs
comme Microsoft Window, les distributions GNU/Linux et Mac OS. Généphrase nécessite uniquement la présence
de Java Runtime Environment 7 ou toute version plus récente. Vous pouvez le télécharger sur http://www.java.com/.

Développé par
- Adrien Pavie <panierAvide at laposte dot net>



= Instructions pour l'installation et le lancement =

Généphrase ne nécessite aucune installation pour fonctionner. Double-cliquez simplement sur genephrase-1.0.jar
ou lancez la commande suivante dans un terminal:
`java -jar genephrase-1.0.jar`

Pour afficher de l'aide (et la liste des options disponibles), lancez cette commande:
`java -jar genephrase-1.0.jar console --help`


Vous pouvez charger un dictionnaire XML pour avoir une bonne base pour génèrer vos phrases. L'archive téléchargée
contient un fichier nommé `dela-fr-public-u8.dic.xml`. Il s'agit d'un dictionnaire XML pour la langue française.
Il est complètement compatible avec Généphrase.
Pour le charger en mode fenêtre, faites "Fichier > Charger un dictionnaire" et sélectionnez ce fichier.
Pour le charger en mode console, ajoutez l'option `--load <file>` à la fin de la commande de lancement du
programme (<file> doit pointer sur le dictionnaire souhaité).

= Instructions de compilation =

__Vous devez suivre les instructions suivantes uniquement si vous souhaitez compiler Généphrase depuis le code source.__

Généphrase utilise le logiciel Ant pour lancer automatiquement la compilation, les tests et démarrer l'application.
Vous devez l'installer avant de tenter de compiler le code source. Vous pouvez le télécharger sur http://ant.apache.org/.

Le fichier build.xml est situé dans le dossier ant/. Pour voir la liste des commandes disponibles, lancez dans
un terminal :
`cd ant/`
Puis
`ant -p`

Avant de lancer la compilation, vous devez initialiser certains paramètres. Lancez la commande :
`ant init`

Puis, pour compiler, lancez :
`cd ../ww/`
`ant compile`

Pour finir, pour démarre Généphrase, lancez :
`ant run`

Ou pour démarrer en mode console :
`ant runConsole`

C'est tout.



= Développement =

Si vous êtes intéressés par Généphrase et que vous souhaitez rejoindre l'équipe de développement,
consultez https://bitbucket.org/panierAvide/genephrase/overview. Vous y trouverez plus d'informations quant au développement.



= Acceptation de la licence =

Avant d'utiliser Généphrase, vous devez consulter et accepter la licence GNU General Public License version 3 (voir LICENSE.txt).

NOTE: Ce programme inclus des ressources externes :
   * data/dela-fr-public-u8.dic.xml - Dictionnaire XML Français (by LIGM, Équipe d'informatique linguistique)
                                      Téléchargement: http://infolingu.univ-mlv.fr/DonneesLinguistiques/Dictionnaires/telechargement.html
                                      Licence:        LGPLLR (http://infolingu.univ-mlv.fr/DonneesLinguistiques/Lexiques-Grammaires/lgpllr.html)
 
